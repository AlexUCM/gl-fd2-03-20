console.group("Topic: Strings");

// Task 01. padStart
// RU: Объявите три переменных: hour, minute, second.
//     Присвойте им следующие значения: 4, 35, 5.
//     Выведите в консоль время в формате 04:35:05.
// EN: Declare three variables: hour, minute, second.
//     Assign them the following values: 4, 35, 5.
//     Display the time in the format 04:35:05 in the console.

{
    const hour = 4;
    const minute = 35;
    const second = 5;

    const transformNumbers = number => number.toString().padStart(2, '0');
    console.log(`${transformNumbers(hour)}:${transformNumbers(minute)}:${transformNumbers(second)}`);
}

// Task 02. repeat
// RU: Создайте функцию, которая выведет в консоль пирамиду на 9 уровней как показано ниже
//     1
//     22
//     333
//     4444
//     ...
// EN: Create a function which displays a 9 level pyramid in the console according to the
//     following pattern
//     1
//     22
//     333
//     4444
//     ...

{
    const startNumber = 1;
    const endNumber = 9;

    function renderPyramide(start, end) {
        for (let i = start; i <= end; i++) {
            console.log(i.toString().repeat(i))
        }
    }

    renderPyramide(startNumber, endNumber);
}


// Task 03. includes
// RU: Напишите код, который выводит в консоль true, если строка str содержит
//     'viagra' или 'XXX', а иначе false.
//     Тестовые данные: 'buy ViAgRA now', 'free xxxxx'
// EN: Create a snippet of code which displays the value true in the console
//     when str contains 'viagra' or 'XXX', otherwise it displays false.

// Fixed

{
	const str1 = 'buy ViAgRA now';
    const str2 = 'free xxxxx';

    const testWords = ['viagra', 'XXX'];

    function isStringContainsWords(string, testWordsArray) {
		const entryIndices = testWordsArray.map(word => string.indexOf(word));
		return !entryIndices.includes(-1)
    }

    console.log(isStringContainsWords(str1, testWords));
    console.log(isStringContainsWords(str2, testWords));
}

// Task 04. includes + index
// RU: Проверить, содержит ли строка второе вхождение подстроки,
//     вернуть true/false.
// EN: Check whether the string contains a second occurrence of a substring,
//     return true / false.

{
    const phrase = 'мама мыла маму';

    function isContent2Enters(phrase, string) {
        const firstEnterIndex = phrase.indexOf(string);
        return phrase.includes(string, firstEnterIndex + 1);
    }

    const contentResult = isContent2Enters(phrase, 'мам');
    console.log(contentResult);
}

// Task 05. Template literal
// RU: Создать строку: "ten times two totally is 20"
//     используя переменные:
//     const a = 10;
//     const b = 2;
//     и template literal
// EN: Create s string "ten times two totally is 20"
//     using the following variables:
//     const a = 10;
//     const b = 2;
//     and template literal

{
    const a = 10;
    const b = 2;
    const string = `ten times two totally is ${a * b}`;
    console.log(string);
}

// Task 06. normalize
// RU: Создайте функцию, которая сравнивает юникод строки.
//     Сравните две строки
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';
// EN: Create a function that compares the unicode strings.
//     Compare 2 strings:
//     var str1 = '\u006d\u0061\u00f1';
//     var str2 = '\u006d\u0061\u006e\u0303';

{
    const str1 = '\u006d\u0061\u00f1';
    const str2 = '\u006d\u0061\u006e\u0303';

    function compareUnicodeStrings(str1, str2) {
        if (str1.normalize() !== str2.normalize()) {
            console.log('Строки не равны');
        } else {
            console.log('Строки равны');
        }
    }
    compareUnicodeStrings(str1, str2);
}



// Task 07. endsWith
// RU: Создайте функцию, которая на вход получает массив имен файлов и расширение файла
//     и возвращает новый массив, который содержит файлы указанного расширения.
// EN: Create a function that gets an array of file names and a file extension as its parameters
//     and returns a new array that contains the files of the specified extension.

{
    const namesArr = ['start.exe', 'DSC1029.jpg', 'picture01.png', 'DSC1031.jpg'];
    const extension = 'jpg';

    function createFilesArr(arr, ext) {
        return arr.filter(item => item.endsWith(ext));
    }

    const filesArr = createFilesArr(namesArr, extension);
    console.log(filesArr);
}

// Task 08. String.fromCodePoint
// RU: Создать функцию, которая выводит в консоль строчку в формате 'символ - код'
//     для кодов в диапазоне 78000 - 80000.
// EN: Create a function that displays a line in the format 'character - code' to the console
//     for codes in the range of 78000 - 80000.

{
    function showCode(code) {
        if (code >= 78000 && code <= 80000) {
            const char = String.fromCodePoint(code);
            console.log(`${char} - ${code}`);
        } else {
            console.log('Вы должны ввести код в диапазоне 78000 - 80000');
        }
    } 
    showCode(78339);
    showCode(74000);
}

// Task 09
// RU: Создайте функцию, которая должна выводить в консоль следующую пирамиду
//     Пример:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'
// EN: Create a function that should display the next pyramid in the console
//     Example:
//     pyramid(1) = '#'
//
//     pyramid(2) = ' # '
//                  '###'
//
//     pyramid(3) = '  #  '
//                  ' ### '
//                  '#####'

{
    function renderPyramide(char, levels) {
        let increment = -2;
        for (let i = levels; i > 0; i--) {
        increment += 2;
        console.log(' '.repeat(i-1) + char + char.repeat(increment) + ' '.repeat(i-1));
        }
    }
    renderPyramide('#', 1);
    renderPyramide('#', 2);
    renderPyramide('#', 3);
}

// Task 10
// RU: Создайте тег-функцию currency, которая формитирует числа до двух знаков после запятой
//     и добавляет знак доллара перед числом в шаблонном литерале.
// EN: Create a currency tag function that forms numbers up to two decimal digits.
//     and adds a dollar sign before the number in the template literal.

{
    const currency = (wordsArr, floatNumber) => '$' + floatNumber.toFixed(2);
  
    const tagCall = currency`You need ${23.4523}`;
    console.log(tagCall)
}

console.groupEnd();
