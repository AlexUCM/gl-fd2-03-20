console.group("Topic: Arrays");

// Task 01
// RU: Создать массив. Получить последний элемент массива.
//     1.    Без удаления этого элемента из массива.
//     2.    С удалением этого элемента из массива
//    Выведите массивы в консоль
// EN: Create an array of any elements. Get the last element from this array.
//     1.    without deleting this element from an array;
//     2.    with deleting this element from an array.
//     Display them in the console.

{
    const array = [1, 2, 3, 4, 5];
    const lastElement = array[array.length-1];
    console.log(lastElement, array);

    const popElement = array.pop()
    console.log(popElement, array);
}

// Task 02
// RU: Создать массив любых элементов. Добавить элемент в конец массива.
//     1. Модифицировать текущий массив
//     2. Создать новый массив
//     Выведите массивы в консоль
// EN: Create an array of any elements. Add new element to the end of this array.
//     1. mutate current array;
//     2. create a new array.
//     Disply them in the conole.

{
    const array = [1, 2, 3, 4, 5];
    array.push(6);
    console.log(array);

    const copiedArray = [...array, 7];
    console.log(copiedArray);
}

// Task 03
// RU: Создать массив любых элементов. Вставить новый элемент под индексом 3.
//     1. Модифицировать текущий массив
//     2. Создать новый массив
//     Выведите массивы в консоль
// EN: Create an array of any elements. Insert a new element with index 3.
//     1. mutate current array;
//     2. create a new array.
//     Disply them in the conole.

{
    const array = [1, 2, 3, 4, 5];
    array.splice(3, 0, 'a');
    console.log(array);

    const firstPart = array.slice(0, 3);
    const secondPart = array.slice(3)
    firstPart.push('b');
    const newArray = firstPart.concat(secondPart);
    console.log(newArray);
}


// Task 04
// RU: Создать массив любых элементов.
//     Обойти элементы массива и вывести их в консоль.
// EN: Create an array of any elements.
//     Iterate over this array and display each element in the console.

{
    const array = [1, 2, 3, 4, 5];
    array.forEach(elem => console.log(elem))
}

// Task 05
// RU: Создать массив чисел в диапазоне от 0 до 100.
//     Подсчитать и вывети сумму тех элементов, которые больше 50.
// EN: Create an array of numbers in the range from 0 to 100.
//     Calculate and display the sum of the elements, which are greater than 50.

{
    function getArr(start, end) {
        const arr = [];
        let i = start;
        while (i <= end) {
            arr.push(i);
            i++;
        }
        return arr;
    }

    const sum = getArr(0, 100).reduce((total, amount) => amount >= 50 ? total + amount : 0)
    console.log(sum)
}

// Task 06
// RU: Создать массив строк. На основе этого массива создать строку –
//     объдинить все элементы массива, используя определенный разделитель.
// EN: Create an array of strings. Create a string on the basis of this array.
//     This string should contain all elements from an array separated by certain delimeter.

{
    const strArr = ['one', 'two', 'three', 'four'];
    const string = strArr.join(',');
    console.log(string);
}

// Task 07
// RU: Создать массив чисел от 1 до 20 в случайном порядке.
//     Отcортировать массив по возрастанию. Вывести его в консоль.
//     Получить массив, отсортрованный в обратном порядке, и вывести его в консоль.
// EN: Create an array of numbers in the range from 1 to 20 in random order.
//     Sort this array in ascending order. Display it in the console.
//     Create a copy of the previous array in reverse order. Disply it in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
        return arr.sort(() => 0.5 - Math.random())
    }

    const randomOrderArr = getRandomOrderArr(1, 20);
    const sortedArr = randomOrderArr.sort((a, b) => a - b);
    console.log(sortedArr);

    const reversedArray = sortedArr.reverse();
    console.log(reversedArray);
}

// Task 08
// RU: Создать массив [3, 0, -1, 12, -2, -4, 0, 7, 2]
//     На его основе создать новый массив [-1, -2, -4, 0, 0, 3, 12, 7, 2].
//     первая часть - отрицательные числа в том же порядке
//     вторая часть - нули
//     третья часть - положительные числа в том же порядке.
// EN: Create the array: [3, 0, -1, 12, -2, -4, 0, 7, 2]
//     Use this array and create new one: [-1, -2, -4, 0, 0, 3, 12, 7, 2].
//     First part - negative values from the original array in the same order,
//     Next part - zeroes
//     Last part - positive values from the original array in the same order.

{
    const array = [3, 0, -1, 12, -2, -4, 0, 7, 2];

    function getModifiedArr(arr) {
        arr.sort((a, b) => (a <= 0) ? a - b : 0);
        arr.sort((a, b) => (a < 0) ? b - a : 0);
        return arr;
    }

    console.log(getModifiedArr(array));
}


// Task 09
// RU: 1. Создайте массив styles с элементами "Jazz", "Blues".
//     2. Добавьте в конец значение "Rock-n-Roll".
//     3. Замените предпоследнее значение с конца на "Classics".
//     4. Удалите первый элемент из массива и выведите его в консоль.
//     5. Добавьте в начало два элемента со значениями "Rap" и "Reggae".
//     6. Выведите массив в консоль.
// EN: 1. Create an array styles with two elements "Jazz", "Blues".
//     2. Add new element "Rock-n-Roll" to the end of the array.
//     3. Replace the last but one element with new value "Classics".
//     4. Remove the first element from the array and disply it in the console.
//     5. Add two new elements "Rap" and "Reggae" at the begining of the array.
//     6. Display an array in the console.

{
    const styles = ["Jazz", "Blues"];
    styles.push("Rock-n-Roll");
    styles[styles.length-2] = "Classics";
    const shiftedElem = styles.shift();
    console.log(shiftedElem);

    styles.unshift("Rap", "Reggae");
    console.log(styles);
}

// Task 10
// RU: Подсчитать в строке "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh"
//     отдельно количество букв r, k, t и вывести в консоль.
// EN: Calculate the number of letters r, k, t in this string
//     "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh" and display them in the console.

// Fixed

{
    const string = "dskjdhfkjshdfkjhsdkjureyteiruyiqywehjkh";

    function charQuantity(string, letter) {
        const arr = string.split('');
        const quantity = arr.filter(elem => letter === elem).length;
        return quantity;
    }

    console.log(charQuantity(string, 'r'));
    console.log(charQuantity(string, 'k'));
    console.log(charQuantity(string, 't'));
}

// Task 11
// RU: Создать массив любых элементов.
//     Получить случайный элемент из массива и вывести его в консоль.
// EN: Create an array of any elements.
//     Get the random element from this array and display it in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
        return arr.sort(() => 0.5 - Math.random());
    }

    const randomArr = getRandomOrderArr(1, 10)
    
    function getRandomElement(arr) {
        return arr[Math.floor(Math.random() * arr.length)];
    }
    
    const randomElement = getRandomElement(randomArr);
    console.log(randomElement);
}

// Task 12
// RU: Создать двумерный массив:
//     1 2 3
//     4 5 6
//     7 8 9
//     Вывести его в консоль.
// EN: Create two-dementional array:
//     1 2 3
//     4 5 6
//     7 8 9
//     Display it in the console.

{
    const twoLevelsArr = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    console.log(twoLevelsArr);
}

// Task 13
// RU: Преобразовать массив из предыдущего задания в одномерный.
//     Вывести его в консоль
// EN: Transform an array from the previous task into one-dementional array.
//     Display it in the console.

{
    let twoLevelsArr = [[1, 2, 3], [4, 5, 6], [7, 8, 9]];
    const emptyArr = [];
    twoLevelsArr = emptyArr.concat(...twoLevelsArr);
    console.log(twoLevelsArr);
}

// Task 14
// RU: Создать массив любых элементов.
//     На его основе получить новый массив – подмножество элементов
//     оригинального массива начиная с индекса a и заканчивая индексом b.
//     Вывести массив в консоль.
// EN: Create an array of any elements.
//     Create new one on the basis of the originl array. New array should
//     contain elements from index a to index b.
//     Display it in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
        return arr.sort(() => 0.5 - Math.random());
    }

    const randomArr = getRandomOrderArr(1, 10);
    console.log(randomArr);

    function getCustomRangeArr(arr, startIndex, endIndex) {
        return arr.slice(startIndex, endIndex + 1);
    }

    const subsetArr = getCustomRangeArr(randomArr, 3, 5);
    console.log(subsetArr);
}

// Task 15
// RU: Создать массив любых элементов.
//     Найти индекс указаного элемента в массиве и вывести его в консоль.
// EN: Create an array of any elements.
//     Find the index of a particular element in the array and disply it in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
        return arr.sort(() => 0.5 - Math.random());
    }

    const randomArr = getRandomOrderArr(1, 10);
    console.log(randomArr);

    const elementIndex = randomArr.findIndex(item => item === 3);
    console.log(elementIndex);
}

// Task 16
// RU: Создать массив с дублями элементов. На его основе создать новый массив
//     уникальных элементов (удалить дубли).
//     Вывести новый массив в консоль.
// EN: Create an array with duplicate elements. Create new one on the basis of the originl array.
//     Remove duplicated elements.
//     Display it in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
            if (i % 3 === 0) {
                arr.push(i);
            }
        }
        return arr.sort(() => 0.5 - Math.random());
    }

    const doublesArr = getRandomOrderArr(1, 10);
    console.log(doublesArr);

    const arrWODoubles = doublesArr.filter((elem, index, arr) => arr.indexOf(elem) === index);
    console.log(arrWODoubles);
}

// Task 17
// RU: Создать массив с дублями. Найти первый элемент, который дублируется.
//     Заменить этот элемент и все его копии на символ '*'.
//     Вывести массив в консоль.
// EN: Create an array with duplicate elements. Find first duplicated element.
//     Replace this element and all its copies with symbol '*'.
//     Display it in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
            if (i % 4 === 0) {
                arr.push(i);
                arr.push(i);
            }
        }
        return arr.sort(() => 0.5 - Math.random());
    }

    const doublesArr = getRandomOrderArr(1, 10);
    console.log(doublesArr);

    const firstDuplicateItem = doublesArr.find((elem, index, arr) => arr.lastIndexOf(elem) !== index);
    console.log(firstDuplicateItem);

    doublesArr.forEach((elem, index, arr) => {
       if (elem === firstDuplicateItem) {
          return arr[index] = '*';
       }
    })
    console.log(doublesArr)
}

// Task 18
// RU: Создать массив целых чисел. На его основе создать массивы – представления
//     этих же чисел в бинарном, восьмеричном и шестнадцатеричном виде.
//     Вывести эти массивы в консоль.
// EN: Create an array of integer numbers. Create 3 new ones on the basis of the originl array.
//     First array contains the binary representation of the elements from the original array.
//     Second array contains the octal representation of the elements from the original array.
//     Third array contains the hexadecimal representation of the elements from the original array.
//     Display them in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
        return arr.sort(() => 0.5 - Math.random());
    }

    const randomArr = getRandomOrderArr(1, 12)
    console.log(randomArr);

    function getArrayWithRadix(arr, radix) {
        return arr.map(elem => elem.toString(radix))
    }

    console.log(getArrayWithRadix(randomArr, 2));
    console.log(getArrayWithRadix(randomArr, 8));
    console.log(getArrayWithRadix(randomArr, 16));
}

// Task 19
// RU: Получить из строки 'a big brown fox jumps over the lazy dog' массив слов,
//     который содержит элементы, длина которых не больше 3 символа.
//     Вывести массив в консоль.
// EN: Get the array of words from the string 'a big brown fox jumps over the lazy dog'.
//     This array should contain only words, the length of which is 3 or less characters.
//     Display it in the console.

{
    const fox = 'a big brown fox jumps over the lazy dog';

    function getArrLessXLetters(string, length) {
        return string.split(' ').filter(elem => elem.length <= length);
    }

    console.log(getArrLessXLetters(fox, 3));
}

// Task 20
// RU: Создать массив, который содержит строки и числа.
//     Проверить, содержит ли массив только строки.
//     Вывети результат в консоль
// EN: Create an array of numbers and strings.
//     Check whether this array contains only strings.
//     Display the result in the console.

{
    function getRandomOrderArr(start, end) {
        const arr = [];
        for (let i = start; i <= end; i++) {
            arr.push(i);
            if (i % 3 === 0) {
                arr.push('someText');
            }
        }
        return arr.sort(() => 0.5 - Math.random());
    }
    const randomArr = getRandomOrderArr(1, 10)
    console.log(randomArr);


    function isOnlyTypeArr(arr, dataType) {
        const check = arr.every(elem => typeof elem === dataType);
        if (check) { 
            console.log(`Массив содержит только элементы типа ${dataType}`)
        } else {
        console.log(`В массиве элементы разных типов`);
        }
    }

    isOnlyTypeArr(randomArr, 'string');
}

// Task 21
// RU: Создать отсортированный массив чисел.
//     Реализовать функцию binarySearch(arr, value), которая принимает массив
//     и значение и возвращает индекс значения в массиве или -1.
//     Функция должна использовать бинарный поиск.
//     Вывести результат в консоль.
// EN: Create an array of numbers in sort order.
//     Implement function binarySearch(arr, value), which takes an array
//     and a value and returns the index of this value in the array or -1.
//     Function should use binary search.
//     Display the result in the console.

{
    function getArr(start, end) {
        const arr = [];
        let i = start;
        while (i <= end) {
            arr.push(i);
            i++;
        }
        return arr;
    }

    const array = getArr(1, 10);
    console.log(array);

    function binarySearch(arr, value) {
        let first = 0;
        let last = arr.length - 1;
        let position = -1;
        let found = false;
        let middle;
      
        while (found === false && first <= last) {
            middle = Math.floor((first + last)/2);
            if (arr[middle] == value) {
                found = true;
                position = middle;
            } else if (arr[middle] > value) {
                last = middle - 1; 
            } else {
                first = middle + 1;
            }
        }
        return position;
    }
    console.log(binarySearch(array, 11))
    console.log(binarySearch(array, 3))
}

console.groupEnd();
