console.group("Topic: Date object");

// Task 1
// RU: Создать текущую дату и вывести ее в формате dd.mm.yyyy и dd Month yyyy
// EN: Create current date and display it in the console according to the format
//     dd.mm.yyyy и dd Month yyyy

{
    const dateObj = new Date();

    const monthsArr = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
    ]

    const day = dateObj.getDate();
    const month = dateObj.getMonth();
    const year = dateObj.getFullYear();

    const currentDate = `${day}.${month}.${year}`;
    console.log(currentDate);

    const currentDateWithMonth = `${day} ${monthsArr[month]} ${year}`;
    console.log(currentDateWithMonth);
}

// Task 2
// RU: Создать объект Date из строки '15.03.2025'.
// EN: Create an object Date from the string '15.03.2025'.

{
    const string = '15.03.2025';
  
    function getDateString(str) {
        const dateArr = str.split('.');
        const [day, month, year] = dateArr;
        return `${year}-${month}-${day}`
    }
  
    const formattedDateString = getDateString(string);
    console.log(formattedDateString);
    
    const date = new Date(formattedDateString);
    console.log(date)
}

// Task 3
// RU: Создать объект Date, который содержит:
//     1. завтрашнюю дату,
//     2. первое число текущего месяца,
//     3. последнее число текущего месяца
// EN: Create an object Date, which represents:
//     1. tomorrow
//     2. first day of the current month
//     3. last day of the current month

{
    const currentDate = new Date();
    const date = currentDate.getDate();
    const month = currentDate.getMonth();
  
    const tomorrow = new Date();
    tomorrow.setDate(date + 1);
    console.log(tomorrow);
  
    const firstDay = new Date();
    firstDay.setDate(1);
    console.log(firstDay);
  
    const lastDay = new Date();
    lastDay.setMonth(month+1)
    lastDay.setDate(0);
    console.log(lastDay);
}

// Task 4
// RU: Подсчитать время суммирования чисел от 1 до 1000.
// EN: Calculate the time of summing numbers from 1 to 1000.

{
    function getSumBench(firstNum, endNum) {
        let sum = 0;
        const start = Date.now();
        for (let i = firstNum; i < endNum; i++) {
            sum += i;
        }
        const end = Date.now();
        return end - start;
    }
  
    console.log(getSumBench(1, 1000));
}

// Task 5
// RU: Подсчитать количество дней с текущей даты до Нового года.
// EN: Calculate the number of days from the current date to the New Year.

{
    function getDaysToNY(year) {
        const newYear= new Date(year, 0, 1, 0, 0, 0, 0);
        const daysLeft = Math.floor((newYear.getTime() - Date.now())/86400/1000);
        console.log(`До нового ${year} года осталось ${daysLeft} дней`)
        return daysLeft;
    }
    
    getDaysToNY(2021)
}

console.groupEnd();
