console.group("Topic: Primitive Data Types");

// Task 01
// Объявите переменную days и проинициализируйте ее числом от 1 до 10.
// Преобразуйте это число в количество секунд и выведите в консоль.

{
    const days = 2;
    const seconds = days * 86400; // 86400 seconds in 1 day
    console.log(seconds);
}


// Task 02
// Объявите две переменные: admin и name. Установите значение переменной name
// в ваше имя. Скопируйте это значение в переменную admin и выведите его в консоль.

{
    let admin;
    const name = 'Alexander';
    admin = name;
    console.log(admin);
}

// Task 03
// Объявите три переменных: a, b, c. Присвойте им следующие значения: 10, 2, 5.
// Объявите переменную result и вычислите сумму значений переменных a, b, c.
// Объявите переменную min и вычислите минимальное значение переменных a, b, c.
// Выведите результат в консоль.

{
    const a = 10;
    const b = 2;
    const c = 5;
    const result = a + b + c;
    let min;

    if (a > b && c > b) {
        min = b;
    } else if (a > c) {
        min = c;
    } else {
        min = a;
    }
    console.log(result, min);
}

// Task 04
// Объявите три переменных: hour, minute, second. Присвойте им следующие значения:
// 10, 40, 25. Выведите в консоль время в формате 10:40:25.

{
    const hour = 10;
    const minute = 40;
    const second = 25;
    console.log(`${hour}:${minute}:${second}`);
}

// Task 05
// Объявите переменную minute и проинициализируйте ее целым числом.
// Вычислите к какой четверти принадлежит это число и выведите в консоль.

{
    const minute = 32;

    function getQuarter(minute) {
        let quarter = 0;
        while (minute >= 0) {
            minute -= 15;
            quarter++;
        }
        return quarter;
    }

    console.log(getQuarter(minute));
}

// Task 06
// Объявите две переменные, которые содержат стоимость товаров:
// первый товар - 0.10 USD, второй - 0.20 USD
// Вычислите сумму и выведите в консоль. Используйте toFixed()

{
    const firstItemCost = '0.10 USD';
    const secondItemCost = '0.20 USD';
    const itemsSum = (parseFloat(firstItemCost) + parseFloat(secondItemCost)).toFixed();
    console.log(itemsSum);
}

// Task 07
// Объявите переменную a.
// Если значение переменной равно 0, выведите в консоль "True", иначе "False".
// Проверьте, что будет появляться в консоли для значений 1, 0, -3.

{
    function output(a) {
        if (a === 0) {
            console.log("True");
        } else {
            console.log("False");
        }
    }
    output(1);
    output(0);
    output(-3);
}

// Task 08
// Объявите две переменных: a, b. Вычислите их сумму и присвойте переменной result.
// Если результат больше 5, выведите его в консоль, иначе умножте его на 10
// и выведите в консоль.
// Данные для тестирования: 2, 5 и 3, 1.

{
    function sum(a, b) {
        let result = a + b;
        if (result > 5) {
            console.log(result);
        } else {
            console.log(result * 10);
        }
    }
    sum(2,5);
    sum(3,1);
}

// Task 09
// Объявите переменную month и проинициализируйте ее числом от 1 до 12.
// Вычислите время года и выведите его в консоль.

{
    const month = 7;

    switch (month) {
        case 1:
        case 2:
        case 12:
            console.log(`${month} месяц - это зима`);
            break;
        case 3:
        case 4:
        case 5:
            console.log(`${month} месяц - это весна`);
            break;
        case 6:
        case 7:
        case 8:
            console.log(`${month} месяц - это лето`);
            break;
        default:
            console.log(`${month} месяц - это осень`);
    }
}

// Task 10
// Выведите в консоль все числа от 1 до 10.

// Fixed

{
    const start = 1;
    const end = 10
    let conunter = start;

    do {
        console.log(conunter);
        conunter++;
    }
    while (conunter <= end);
}

// Task 11
// Выведите в консоль все четные числа от 1 до 15.

{
    function oddNumbers(start,end) {
        let conunter = start;
        while (conunter <= end) {
            if (conunter % 2 === 0) {
                console.log(conunter);
                conunter +=2;
            } else {
                conunter++;
            }
        }
    }
}
oddNumbers(1, 15);

// Task 12
// Нарисуйте в консоле пирамиду на 10 уровней как показано ниже
// x
// xx
// xxx
// xxxx
// ...

{
    let i = 0;
    let symbols = 'x';
    while (i < 10) {
        console.log(symbols);
        i++;
        symbols += 'x';
    }
}

// Task 13
// Нарисуйте в консоле пирамиду на 9 уровней как показано ниже
// 1
// 22
// 333
// 4444
// ...

{
    let row; 

    for (let i = 1; i <= 9; i++) {
        row = i.toString();
        for (let j = 1; j < i; j++) {
            row += i.toString();
        }
        console.log(row);
    }
}


// Task 14
// Запросите у пользователя какое либо значение и выведите его в консоль.

{
    const message = prompt('Введите значение:');
    console.log(message)
}

// Task 15
// Перепишите if используя тернарный опертор
// if (a + b < 4) {
//   result = 'Мало';
// } else {
//   result = 'Много'; 
// }

//Fixed

{
    const a = 1;
    const b = 2;
    const result = (a + b < 4) ? 'Мало' : 'Много';
    console.log(result);
}

// Task 16
// Перепишите if..else используя несколько тернарных операторов.
// var message;
// if (login == 'Вася') {
//   message = 'Привет';
// } else if (login == 'Директор') {
//   message = 'Здравствуйте';
// } else if (login == '') {
//   message = 'Нет логина';
// } else {
//   message = '';
// }

// Fixed

{
    const login = 'Директор';

    const message = 
        (login === 'Вася') 
            ? 'Привет'
                : (login === 'Директор') 
                    ? 'Здравствуйте' 
                        : (login === '') 
                            ?  'Нет логина'
                                : '';
    console.log(message);
}

// Task 17
// Замените for на while без изменения поведения цикла
// for (var i = 0; i < 3; i++) {
//   alert( "номер " + i + "!" );
// }

{
    let i = 0;
    while (i < 3) {
        alert( "номер " + i + "!" );
        i++;
    }
}

// Task 18
// Напишите цикл, который предлагает prompt ввести число, большее 100.
// Если пользователь ввёл другое число – попросить ввести ещё раз, и так далее.
// Цикл должен спрашивать число пока либо посетитель не введёт число,
// большее 100, либо не нажмёт кнопку Cancel (ESC).
// Предусматривать обработку нечисловых строк в этой задаче необязательно.

{
    let enteredNumber;

    do {
        enteredNumber = prompt('Введите число больше 100');
        if (!enteredNumber) {
            break;
        }
    } while (enteredNumber <= 100);
}

// Task 19
// Переписать следующий код используя switch
// var a = +prompt('a?', '');
// if (a == 0) {
//   alert( 0 );
// }
// if (a == 1) {
//   alert( 1 );
// }
// if (a == 2 || a == 3) {
//   alert( '2,3' );
// }

{
    const a = +prompt('a?', '');
    switch (a) {
        case 0:
            alert( 0 );
            break;
        case 1:
            alert( 1 );
            break;
        case 2:
        case 3:
            alert( '2,3' );
            break;
    }
}


// Task 20
// Объявите переменную и проинициализируйте ее строчным значением в переменном
// регистре. (Например так "таООооОддОО")
// Напишите код, который преобразует эту строку к виду:
// первая буква в верхнем регистре, остальные буквы в нижнем регистре.
// Выведите результат работы в консоль
// Используйте: toUpperCase/toLowerCase, slice.

{
    let diffCaseString = 'таООооОддОО';
    const firstLetter = diffCaseString.slice(0,1);
    const leastString = diffCaseString.slice(1);
    diffCaseString = firstLetter.toUpperCase() + leastString.toLowerCase();
    console.log(diffCaseString);
}

// Task 21
// Напишите код, который выводит в консоль true, если строка str содержит
// „viagra“ или „XXX“, а иначе false.
// Тестовые данные: 'buy ViAgRA now', 'free xxxxx'

// Fixed

{
	const str1 = 'buy ViAgRA now';
    const str2 = 'free xxxxx';

    const testWords = ['viagra', 'XXX'];

    function isStringContainsWords(string, testWordsArray) {
		const entryIndices = testWordsArray.map(word => string.indexOf(word));
		return !entryIndices.includes(-1)
    }

    console.log(isStringContainsWords(str1, testWords));
    console.log(isStringContainsWords(str2, testWords));
}

// Task 22
// Напишите код, который проверяет длину строки str, и если она превосходит
// maxlength – заменяет конец str на "...", так чтобы ее длина стала равна maxlength.
// Результатом должна быть (при необходимости) усечённая строка.
// Выведите строку в консоль
// Тестовые данные:
//  "Вот, что мне хотелось бы сказать на эту тему:", 20
//  "Всем привет!", 20

// Fixed

{
    const str1 = "Вот, что мне хотелось бы сказать на эту тему:";
    const str2 = "Всем привет!";
    const strMaxLength = 20;

    function addPointsToStringIfItTooLong(string, maxlength) {
        if (string.length > maxlength) {
            const diff = string.length - maxlength;
            const firstPart = string.slice(0, maxlength);
            let secondPart = '';
            for (let i = 1; i <= diff; i++) {
                secondPart += '.'
            }
            return firstPart + secondPart;
        } else {
            return string;
        }
    }

    console.log(addPointsToStringIfItTooLong(str1, strMaxLength));
    console.log(addPointsToStringIfItTooLong(str2, strMaxLength));
}


// Task 23
// Напишите код, который из строки $100 получит число и выведите его в консоль.

// Fixed

{
    const string = '$100';

    function getNumber(string) {
        const number = string.replace(/\D+/g, '')
        return parseInt(number);
    }

    console.log(getNumber(string));
}

// Task 24
// Напишите код, который проверит, является ли переменная промисом

{
    const prom = new Promise((res, rej) => res("done"));
    const obj = {};
  
    function isPromise(instance) {
        console.log(Promise.resolve(instance)=== instance);
    }
    isPromise(prom);
    isPromise(obj);
}

console.groupEnd();
