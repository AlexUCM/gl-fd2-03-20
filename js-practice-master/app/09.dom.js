console.group("Topic: DOM");

// Task 01
// Найти элемент с id= "t01". Вывести в консоль.
// Найти родительский элемент и вывести в консоль.
// Найти дочерние ноды, если они есть, и вывести в консоль
// названия и тип нод.

//Fixed

{
    const taskDiv = document.querySelector('#t01');
    console.log(taskDiv);
    console.log(taskDiv.parentElement);
	taskDiv.childNodes.forEach(node => console.log(node.nodeName));
	taskDiv.childNodes.forEach(node => console.log(node.textContent));


// Task 02
// Подсчитать количество <li> элементов на странице. Для поиска элементов использовать
// getElementsByTagName(). Вывести в консоль.
// Добавить еще один элемент в список и вывести снова их количество.


    const liElemsByTag = taskDiv.getElementsByTagName('li');
    console.log(liElemsByTag.length);
    const unorList = taskDiv.querySelector('#u01');
    const newLi1 = document.createElement('li');
    unorList.appendChild(newLi1);
    console.log(liElemsByTag.length);


// Task 03
// Получить элементы <li> используя метод querySelectorAll() и вывети их в консоль
// Добавить новый <li> и снова вывести в консоль


    const liElemsByQuery = taskDiv.querySelectorAll('li');
    console.log(liElemsByQuery.length);
    const newLi2 = document.createElement('li');
    unorList.appendChild(newLi2);
    console.log(liElemsByQuery.length);
}

// Task 04
// Найти все первые параграфы в каждом диве и установить цвет фона #ffff00

{
    const divList = document.querySelectorAll('.t04');
    console.dir(divList)
    divList.forEach(elem => elem.querySelector('p').style.backgroundColor ='#ffff00')
}

// Task 05
// Подсчитать сумму строки в таблице и вывести ее в последнюю ячейку

{
    const tableRow = document.querySelector('tr');
    const tableCells = tableRow.querySelectorAll('td');

    const sum = [];
    tableCells.forEach(elem => sum.push(parseFloat(elem.innerHTML)));

    const number = sum
        .filter(item => item)
        .reduce((acc, item) =>  acc + item, 0);

    const result = Math.round(number).toString();
    console.log(result)
    tableRow.lastChild.textContent = result;
}

// Task 06
// Вывести значения всех атрибутов элемента с идентификатором t06

// Fixed

{
    const elem = document.querySelector('#t06');
    for (let attr of elem.attributes) {
        console.log( `${attr.name} = ${attr.value}` );
    }


// Task 07
// Получить объект, который описывает стили, которые применены к элементу на странице
// Вывести объект в консоль. Использовать window.getComputedStyle().

    const styles = window.getComputedStyle(elem);
    console.log(styles)
}

// Task 08
// Установите в качестве контента элемента с идентификатором t08 следующий параграф
// <p>This is a paragraph</>

// Fixed

{
    const elem1 = document.querySelector('#t08');
    elem1.innerHTML = '<p>This is a paragraph</p>'

// Task 09
// Создайте элемент <div class='c09' data-class='c09'> с некоторым текстовым контентом, который получить от пользователя,
// с помощью prompt, перед элементом с идентификатором t08,
// когда пользователь кликает на нем

    const elem2 = document.createElement('div');
    elem2.setAttribute('class', 'c09');
    elem2.setAttribute('data-class', 'c09');
    const content = prompt('Введите значение: ');
    elem2.textContent = content;
    elem1.before(elem2);

}

// Task 10
// Удалите у элемента с идентификатором t06 атрибут role
// Удалите кнопку с идентификатором btn, когда пользователь кликает по ней

{
    const elem = document.querySelector('#t06');
    elem.removeAttribute('role')
    const button = document.querySelector('#btn');
    button.addEventListener('click', () => button.remove())
}

console.groupEnd();
