console.group("Topic: Objects");

// Task 01
// RU: Создать функцию-конструктор Tune(title, artist) для создания объектов
//     с публичными свойствами title, artist и методом concat().
//     Метод должен возвращать конкатенацию значений свойств title и artist.
//     Создать несколько объектов. Вызвать их метод concat().
// EN: Create function-constructor Tune(title, artist) for creating objects
//     with public properties title, artist and method concat().
//     Mathod should return the concatenation of values of propeties title and artist.
//     Create a few objects. Call their method concat().

{
    function Tune(title, artist) { 
        this.title = title;
        this.artist = artist;
        this.concat = function() { 
            return this.title + ' / ' +  this.artist; 
        }
    }
        
    const pugach = new Tune('Миллион роз', 'Алла Пугачёва');
    console.log(pugach.concat());

    const kalina = new Tune('За пацанов', 'Виктор Калина');
    console.log(kalina.concat());

    const cann = new Tune('Hammer Smashed Face', 'Cannibal Corpse');
    console.log(cann.concat());
}

// Task 02
// RU: Создать функцию-конструктор Tune(title, artist) для создания объектов
//     с приватными свойствами title, artist и публичным методом concat().
//     Метод должен возвращать конкатенацию значений свойств title и artist.
//     Создать несколько объектов. Вызвать их метод concat().
// EN: Create function-constructor Tune(title, artist) for creating objects
//     with private properties title, artist and method concat().
//     Mathod should return the concatenation of values of propeties title and artist.
//     Create a few objects. Call their method concat().


{
    function Tune(title, artist) {                      
        this.concat = function() { 
            return title + ' / ' + artist; 
        }
    }

    const pugach = new Tune('Миллион роз', 'Алла Пугачёва');
    console.log(pugach.concat());

    const kalina = new Tune('За пацанов', 'Виктор Калина');
    console.log(kalina.concat());

    const cann = new Tune('Hammer Smashed Face', 'Cannibal Corpse');
    console.log(cann.concat());
}

// Task 03
// RU: Расширить прототип объекта String методом exclaim() если его нет в прототипе.
//     Метод должен добавлять знак восклицания к строке и выводить ее в консоль.
// EN: Extend the prototype of object String with the method exclaim(), if it doesn't exist.
//     Method should add exclaimation mark to the string and disply it in the console.

// Fixed

{   
    if (!String.prototype.hasOwnProperty('exclaim')) {
          String.prototype.exclaim = function() {
              return this + '!';
            };
      }
      const test = 'Test string';
      console.log(test.exclaim());
}

// Task 04
// RU: Создать функцию-конструктор Book(title, author).
//     Добавить два метода: getTitle, getAuthor.
//     Создать функцию-конструктор TechBook(title, author, category).
//     Передать значения title, author функции-конструктору Book.
//     Добавить два метода: getCategory, getBook – возвращает строку со значениями параметров.
//     Для реализации наследования используйте:
//     1. Object.create()
//     2. Class
// EN: Create function-constructor Book(title, author).
//     Add two methods: getTitle, getAuthor.
//     Create function-constructor TechBook(title, author, category).
//     Pass the value of title, author to the function-constructor Book.
//     Add two methods: getCategory, getBook - returns the string with values of all parameters.
//     Implement inheritance using:
//     1. Object.create()
//     2. Class

// Fixed

{
    const Book = function(title, author) {                      
        this.getTitle = function() { 
            return title;
        }
        this.getAuthor = function() { 
            return author;
        }
    }

    const TechBook = function(title, author, cathegory) { 
        this.title = title;
        this.author = author;
        this.getCathegory = function() { 
            return cathegory;
        }
        this.getBook = function() {
            return `${this.title} / ${this.author} / ${this.getCathegory()}`;
        }

        Book.apply(this, [title, author])
    }
    
    TechBook.prototype = Object.create(Book.prototype)
    TechBook.prototype.constructor = TechBook;
    const book = new TechBook('Свидание с Рамой', 'Артур Кларк', 'Фантастика');
    console.log(book);
    console.log(book.getBook());
}

// Task 05
// RU: Создайте класс Shape со статическим свойством count.
//     Проинициализируйте это свойство 0.
//     В конструкторе класса увеличивайте count на 1.
//     Создайте производный класс Rectangle. Добавьте метод для подсчета площади.
//     Создайте несколько объектов. Выведите в консоль количество созданных объектов.
// EN: Create class Shape with static property count.
//     Initialize the property count with 0.
//     Increment the value of count by 1 in the constructor.
//     Create derived class Rectangle. Add method to calculate area.
//     Create a few objects. Display the number of created objects in the console.

{
    class Shape {
        static count = 0;
        constructor() {
            Shape.count++;
        }
    }

    class Rectangle extends Shape {
        constructor() {
            super();
        }
        getArea(a, b) {
            return a * b;
        }
    }

    new Rectangle();
    new Rectangle();
    new Rectangle();
    console.log(Shape.count);
}

// Task 06
// RU: Создать функцию-конструктор Person() для конструирования объектов.
//     Добавить два метода: setFirstName() и setLastName().
//     Методы должны вызываться в цепочке, например obj.setFirstName(...).setLastName(...)
// EN: Create function-constructor Person() for creating objects.
//     Add two methods: setFirstName() и setLastName().
//     These methods should be called in chain like this obj.setFirstName(...).setLastName(...)

{
    function Person() { 
        this.setFirstName = function(name) { 
            this.firstName = name;
            return this;
        }
        this.setLastName = function(name) { 
            this.lastName = name;
            return this;
        }
    }

    const man = new Person();
    man.setFirstName('Ivan').setLastName('Ivanov');
    console.log(man)
}

// Task 07
// RU: Cоздать объект data и сконфигурирвать его свойства:
//     1. id: значение = 1, изменяемое.
//     2. type: значение = 'primary', перечисляемое
//     3. category: getter возвращает значение поля _category,
//                  setter устанавливает значение поля _category, перечисляемое, конфигурируемое.
//     Используя for-in вывести свойства объекта в консоль
// EN: Create an object data and configure its properties:
//     1. id: value = 1, writable
//     2. type: value = 'primary', enumerable
//     3. category: getter returns the value of the property _category,
//                  setter sets the value if the property _category, enumerable, configurable.
//     Using for-in display property of an object in the console.

{
    const data = {};
    Object.defineProperty(data, 'id', {
        value: 1,
        writable: true
    });
    Object.defineProperty(data, 'type', {
        value: 'primary',
        enumerable: true
    });
    Object.defineProperty(data, 'category', {
        get: function() { 
            return this._category; 
        },
        set: function(newValue) { 
            this._category = newValue;
        },
        enumerable: true,
        configurable: true
    });

    data.category = 'test';
    console.log(data.category)

    for (let prop in data) {
        console.log("data." + prop + " = " + data[prop]);
      }
}

// Task 08
// RU: Создать литерал объекта с двумя свойствами. Запретить расширять объект.
// EN: Create object literal with two properties. Deny extend the object.

{
    const person = {
        firstName: 'Ivan',
        lastName: 'Ivanov'
    }
    Object.preventExtensions(person);
    person.age = 20;
    console.log(person);
}

// Task 09 TodoList Application
// RU: Создать классы 'задача' и 'список задач' со следющим функционалом:
//     1. Добавить задачу в список
//     2. Получить и вывести в консоль список всех задач
//        в формате "[new] Task 1", "[completed] Task2"
//     3. Отметить указанную задачу как выполненную
//     4. Удалить задачу из списка
//     5. Отсортировать задачи по алфавиту по возрастанию или по убыванию
//     6. Очистить список задач
// EN: Create classes 'task' and 'task list' with the following features:
//     1. Add task to the list
//     2. Get and display the list of all tasks in the console
//        using the following format "[new] Task 1", "[completed] Task2"
//     3. Check task as a completed task
//     4. Remove task from the list of tasks.
//     5. Sort tasks alphabetically in asc or desc order
//     6. Clear the list of tasks.

// Fixed

{
    class Task {
        constructor(title) {
            this.id = Date.now(),
            this.title = title,
            this.status = 'new';
        }
    }
        
    class TaskList {
        tasks = [];
        constructor() {}
        
        addTask(task) {
            this.tasks.push(task);
            console.log('Task added!');
        }
      
        getTasks() {
            const tasks = this.tasks.map(task => `[${task.status}] ${task.title}`);
            console.log(tasks)
        }
        
        completeTask(index) {
            this.tasks[index].status = 'completed';
            console.log(`Task ${index} is completed`);
        }
      
        deleteTask(index) {
            this.tasks.splice(index, 1);
            console.log(`Task ${index} is deleted`);
        }
      
        sortTasks() {
            this.tasks.sort((a, b) => {
                if (a.title > b.title) {
                    return 1;
                } else if (a.title < b.title) {
                    return -1;
                } else {
                    return 0;
                }
           });
           console.log('Tasks are sorted!')
        }
      
        clearTasks() {
            this.tasks = [];
            console.log('Tasks cleared!');
        }
    }
        
    const t1 = new Task('Buy milk');
    const t2 = new Task('Buy bread')
    const tList = new TaskList();
      
    tList.addTask(t1);
    tList.addTask(t2);
    tList.completeTask(0)
    tList.getTasks()
    tList.deleteTask(0)
    tList.getTasks()
    tList.sortTasks()
    tList.getTasks()
}

console.groupEnd();
