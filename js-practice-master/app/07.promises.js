console.group("Topic: Promises");
// Task 01
// Создайте промис, который постоянно находиться в состоянии pending.
// В конструкторе промиса выведите в консоль сообщение "Promise is created".

{
    new Promise((res, reg) => console.log('Promise is created'));
}

// Task 02
// Создайте промис, который после создания сразу же переходит в состояние resolve
// и возвращает строку 'Promise Data'
// Получите данные промиса и выведите их в консоль

{
    const resolved = new Promise((res) => res('Promise Data'));
    resolved.then(data => console.log(data));
}

// Task 03
// Создайте промис, который после создания сразу же переходит в состояние rejected
// и возвращает строку 'Promise Error'
// Получите данные промиса и выведите их в консоль

{
    const rejected = new Promise((rej) => rej('Promise Error'));
    rejected.catch(err => console.log(err));
}

// Task 04
// Создайте промис, который переходит в состояние resolved через 3с.
// (Используйте setTimeout) и возвращает строку 'Promise Data'
// Получите данные промиса и выведите их в консоль

{
    const timeout = new Promise((res) => setTimeout(() => res('Promise Data'), 3000));
    timeout.then(data => console.log(data));
}

// Task 05
// Создайте литерал объекта handlePromise со следующими свойствами:
// promise, resolve, reject, onSuccess, onError
// Проинициализируйте первые три свойства null,
// а последние два функциями, которые принимают один параметр и выводят
// в консоль сообщения: первая - `Promise is resolved with data: ${paramName}`
// вторая - `Promise is rejected with error: ${paramName}`
// Создайте три кнопки и три обработчика события click для этих кнопок
// Первый обработчик, создает промис, заполняет первые три свойства,
// описаного выше объекта: свойство promise получает новый созданный промис,
// свойства resolve и reject получают ссылки на соответствующие функции
// resolve и reject. Следующие два обработчика запускают методы resolve и reject.

// Fixed

{
    const handlePromise = {                                         
        promise: null,
        resolve: null,
        reject: null,
        onSuccess: function(data) {
            console.log(`Promise is resolved with data: ${data}`)
        },
        onError: function(err) {
            console.log(`Promise is rejected with error: ${err}`)
        }
    }

    const createBtn = document.querySelector('#btn-create-promise'); 
    const resolveBtn = document.querySelector('#btn-resolve-promise'); 
    const rejectBtn = document.querySelector('#btn-reject-promise'); 
     
    createBtn.addEventListener('click', createProm); 
    resolveBtn.addEventListener('click', (event) => handlePromise.resolve('data1')); 
    rejectBtn.addEventListener('click', (event) => handlePromise.reject('data2')); 

     
    function createProm(event) { 
		const promise = new Promise((res, rej) => {
			handlePromise.resolve = res;
			handlePromise.reject = rej;
		})
            .then(handlePromise.onSuccess)
            .catch(handlePromise.onError);
		handlePromise.promise = promise;
		console.log(handlePromise)    
    }
} 

// Task 06
// Используйте предыдущее задание. Продублируйте строчку с методом then

{
    //???
}

// Task 07
// Создайте промис, который через 1 с возвращает строку "My name is".
// Создайте функцию onSuccess, которая получает один параметр,
// прибавляет к нему Ваше имя и возвращает новую строку из функции
// Создайте функцию print, которая выводит в консоль значение своего параметра
// Добавьте два метода then и зарегистрируйте созданные функции.

{
    const name = new Promise((res) => setTimeout(() => res('My name is'), 1000));
    const onSuccess = data => data + ' Alexander';
    const print = data => console.log(data);
    name.then(onSuccess).then(print);
}

// Task 08
// Используйте предыдущий код. Добавьте в функци onSuccess генерацию исключения
// Обработайте даное исключение, используя catch. Обратите внимание,
// что метод print при этом не выполняется.

{
    const name = new Promise((res, rej) => setTimeout(() => res('My name is'), 1000));
    function onSuccess(data) {throw 'Ошибка';}
    const print = data => console.log(data);
    name.then(onSuccess).then(print).catch(err => console.log(err));
}

// Task 09
// Напишите функцию getPromiseData, которая принимает один параметр - промис. Функция получает
// значение промиса и выводит его в консоль
// Объявите объект со свойтвом name и значением Anna.
// Создайте врапер для этого объекта и вызовите для него функцию getPromiseData

// Fixed

{
    function getPromiseData(promise) {
        promise.then(data => console.log(data))
    }
    const someObj = {
        name: 'Anna'
    }

    const wrapper = Promise.resolve(someObj); 
    getPromiseData(wrapper);
}

// Task 10
// Создайте два промиса. Первый промис возвращает объект { name: "Anna" } через 2с,
// а второй промис возвращает объект {age: 16} через 3 с.
// Получите результаты работы двух промисов, объедините свойства объектов
// и выведите в консоль

{
    Promise.all([
        new Promise((res, rej) => setTimeout(() => res({ name: "Anna" }), 2000)),
        new Promise((res, rej) => setTimeout(() => res({ age: 16 }), 3000))
    ])
        .then(arr => Object.assign({}, arr[0], arr[1]))
        .then(obj => console.log(obj))
}

// Task 11
// Используйте предыдущее задание. Пусть теперь второй промис переходит в
// состояние rejected со значением "Promise Error". Измените код, чтобы обработать
// эту ситуацию.

{

    Promise.allSettled([
        new Promise((res, rej) => setTimeout(() => res({ name: "Anna" }), 2000)),
        new Promise((res, rej) => setTimeout(() => rej('Promise Error'), 3000))
    ])
        .then(arr => arr[0].value)
        .then(obj => console.log(obj)) 
}

// Task 12
// Создайте промис, который перейдет в состояние resolve через 5с и вернет строку
// 'Promise Data'.
// Создайте второй промис, который перейдет в состояние rejected по клику на
// кнопку. Добавьте обработчик для кнопки.
// Используя метод race организуйте отмену промиса.

{
    const a = new Promise((res, rej) => setTimeout(() => res('Promise Data'), 5000));
    let rejectB;
    const b = new Promise((res, rej) => rejectB = rej);
    
    const cancelButton = document.querySelector('.btn-cancel-promise'); 
    cancelButton.addEventListener('click', () => rejectB('Отклонил b'));

    Promise.race([a, b])
	    .then(value => console.log(value))
	    .catch(err => console.warn(err))
}

console.groupEnd();
