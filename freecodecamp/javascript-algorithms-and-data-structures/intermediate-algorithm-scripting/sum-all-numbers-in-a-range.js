function sumAll(arr) {

    function decrement(condition,number) {
      if (condition === number) {
        return 0;
      } 
      else {
        return number + decrement(condition,number-1);
      }
    }
  
    if (arr[0]>arr[1]) {
      return arr[1] + decrement(arr[1],arr[0]);
    }  
    else if (arr[0]<arr[1]) {
      return arr[0] + decrement(arr[0],arr[1]);
    }
    else {
      return arr[0] + arr[1];
    }
  }
  
  console.log(sumAll([1,4]));
  console.log(sumAll([4,1]));
  console.log(sumAll([5,10]));
  console.log(sumAll([10,5]));