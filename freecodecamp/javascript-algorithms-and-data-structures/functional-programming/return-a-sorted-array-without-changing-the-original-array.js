var globalArray = [5, 6, 3, 2, 9];
function nonMutatingSort(arr) {
  // Add your code below this line

  const swapArr = [...arr];
  return swapArr.sort();

  // Add your code above this line
}
nonMutatingSort(globalArray);
