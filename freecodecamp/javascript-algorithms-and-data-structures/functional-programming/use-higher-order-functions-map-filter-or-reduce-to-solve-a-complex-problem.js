const squareList = (arr) => {
    // only change code below this line
    return arr.filter(item => item > 0 && item % parseInt(item) === 0)
    .map(item => item * item);
  
    // only change code above this line
  };
  
  // test your code
  const squaredIntegers = squareList([-3, 4.8, 5, 3, -3.2]);
  console.log(squaredIntegers);
  
  