function createTaskbar() {

    const taskbar = createDiv('taskbar');

    const form = createForm('addtask-form', 'addTaskForm');

    const titleInput = createInput('addtask-title', 'text', 'title', 'addtask-title', true);
    titleInput.setAttribute('placeholder', 'Добавить задачу');
    titleInput.setAttribute('autocomplete', 'off');

    const button = createButton('important-addtask-btn', 'button', 'importantButton', 'Важное');
    const importantBtn = createImportantButton(button, false);
    importantBtn.addEventListener('click', changeStateImportantButton)

    const deadlineInput = createInput('addtask-date', 'text', 'date', 'addtask-deadline', false);
    deadlineInput.setAttribute('placeholder', 'Дата выполнения');
    deadlineInput.setAttribute('autocomplete', 'off')
    $(deadlineInput).datepicker(datepickerOptions);

    const addTaskBtn = createButton('addtask-btn', 'submit', 'submit', 'Добавить');

    form.append(titleInput, importantBtn, deadlineInput, addTaskBtn);
    form.addEventListener('submit', addTaskAction);

    const activeTasksList = createUl('taskbar-active-tasks');
    const unactiveTasksList = createUl('taskbar-unactive-tasks');

    const hideTasksBtn = createButton('taskbar-hide-btn','button', 'hide-tasks', 'Показать завершённые задачи');
    hideTasksBtn.addEventListener('click', showUnactiveTasks);

    taskbar.append(form, activeTasksList, hideTasksBtn, unactiveTasksList);

    return taskbar;
}

// Event handlers

function changeStateImportantButton(event) {
    if (event.target.classList.contains('important-btn-selected')) {
        event.target.setAttribute('title','Добавить в важные')
    } else {
        event.target.setAttribute('title','Удалить из важных')
    }
    event.target.classList.toggle('important-btn-selected');
}

function showUnactiveTasks(event) {
    const container = event.target.closest('.taskbar');
    const unactiveTaskList = container.querySelector('.taskbar-unactive-tasks');
    unactiveTaskList.classList.toggle('taskbar-unactive-tasks-visible');
    event.target.classList.toggle('taskbar-hide-btn-active');
}

function addTaskAction(event) {
    event.preventDefault();

    const form = event.target.elements;

    const parseDate = form.date.value.split('.')
    const [day, month, year] = parseDate;
    const deadline = new Date(`${year}-${month}-${day}`).getTime();

    const title = form.title.value;
    const id = Date.now();
    const creationDate = id;

    const customCathegories = document.querySelector('.sidebar-custom-cathegories');
    const selectedCathegory = customCathegories.querySelector('.sidebar-selected-cathegory');

    let cathegory;
    if (selectedCathegory) {
        const cathegoryId = selectedCathegory.getAttribute('id');
        cathegory = [+cathegoryId];
    } else {
        cathegory = [];
    }

    let isImportant;
    if (form.importantButton.classList.contains('important-btn-selected')) {
        isImportant = true;
    } else {
        isImportant = false;
    }

    const data = {
        id,
        title,
        creationDate,
        deadline,
        cathegory,
        isImportant,
        isDone: false
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }

    fetch(BASEURL + 'task', requestOptions)
        .then(res => {
            if (res.status === 201) {
                const selectedCathegory = document.querySelector('.sidebar-selected-cathegory')
                changeActiveMenuItem(selectedCathegory)
                form.importantButton.classList.remove('important-btn-selected')
                form.importantButton.setAttribute('title','Добавить в важные')
                form.title.value = '';
            }
        })
}