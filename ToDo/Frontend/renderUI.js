// Constants

const BASEURL = 'http://localhost:3000/';

const datepickerOptions = {
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    nextText: 'Следующий месяц',
    prevText: 'Предыдущий месяц',
    monthNames : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
    dayNamesMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
}

// RenderUI

checkLoginStatus();

function checkLoginStatus() {

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
    }

   fetch(BASEURL + 'check-auth', requestOptions)
        .then(res => {
            if (res.status === 202) {
                return loginActions();
            }
            if (res.status === 403) {
                const body = document.body;
                body.prepend(createLoginWindow());
            }
        })
}

function loginActions() {
    const body = document.body;
    const rootContainer = createRootContainer();
    const logo = createLogo();
    const optbar = createOptBar();
    const editbar = createEditBar();
    const sidebar = createSideBar();
    const taskbar = createTaskbar();

    rootContainer.append(logo, optbar, sidebar, taskbar, editbar);
    body.append(rootContainer);
    renderDefaultTasks();
}

function renderDefaultTasks() {
    const defaultCathegory = document.querySelector('#all-tasks');
    changeActiveMenuItem(defaultCathegory)
}

