function createOptBar() {
    
    const optbar = createDiv('optbar');

    const cathegoryTitle = createSpan('optbar-cathegory-title');
    const optionsGroup = createDiv('optbar-options-group');

    const sortMenu = createDiv('sort-menu');

    const sortButton = сreateSortButton();
    const sortList = createUl('sort-list');

    const sortByCreationDate = createSortMenuItem('sort-type','byCreationDate', 'По дате создания');
    sortByCreationDate.addEventListener('click', setSortType);
    const sortByDeadline = createSortMenuItem('sort-type','byDeadline', 'По дате выполнения');
    sortByDeadline.addEventListener('click', setSortType);
    const sortByName = createSortMenuItem('sort-type','byName', 'По алфавиту');
    sortByName.addEventListener('click', setSortType);

    sortList.append(sortByCreationDate, sortByDeadline, sortByName);
    sortMenu.append(sortList, sortButton);

    const logoutButton = createButton('optbar-logout-btn', 'button', 'logoutBth', 'Выйти');
    logoutButton.addEventListener('click', logoutAction);

    optionsGroup.append(sortMenu, logoutButton);
    optbar.append(cathegoryTitle, optionsGroup);

    return optbar;
}

// Event handlers

function closeSortMenuOnClickOutside(event) {
    const sortMenu = document.querySelector('.sort-menu');
    const sortButton = sortMenu.querySelector('.sort-btn');
    const sortList = sortMenu.querySelector('.sort-list');

    const targetMenu = event.target === sortList || sortList.contains(event.target);
    const targetButton = event.target === sortButton;
    const menuIsActive = sortList.classList.contains('sort-list-visible');

    if (!targetMenu && !targetButton && menuIsActive) {
        sortList.classList.toggle('sort-list-visible')
        sortButton.classList.toggle('sort-btn-active');
    }
}

function showSortMenu(event) {
    event.target.classList.toggle('sort-btn-active');
    const sortMenu = event.target.closest('.sort-menu');
    const sortList = sortMenu.querySelector('.sort-list');
    sortList.classList.toggle('sort-list-visible');
}

function setSortType(event) {
    const sortType = event.target.getAttribute('data-sort');

    const data = {sortType};

    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }
    
    fetch(BASEURL + 'options', requestOptions)
        .then(res => {
            if (res.status === 200) {
                const sortMenu = event.target.closest('.sort-menu');
                const sortButton = sortMenu.querySelector('.sort-btn');
                const sortList = sortMenu.querySelector('.sort-list');
                setTextOnSortButton(sortType, sortButton);

                sortList.classList.toggle('sort-list-visible');
                sortButton.classList.toggle('sort-btn-active');

                const selectedCathegory = document.querySelector('.sidebar-selected-cathegory');
                changeActiveMenuItem(selectedCathegory)
            }
        })
}

function logoutAction(event) {
    
    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
    }

    fetch(BASEURL + 'logout', requestOptions)
        .then(res => {
            if (res.status === 200) {
                const rootContainer = event.target.closest('.root');
                const body = rootContainer.closest('body');
                rootContainer.remove();
                body.append(createLoginWindow());
            }
        })
}