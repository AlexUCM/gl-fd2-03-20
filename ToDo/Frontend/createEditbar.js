function createEditBar(task = null) {

    const editbar = createDiv('editbar');

    const form = createForm('editbar-form', 'editbarForm');

    const titleInput = createInput('editbar-title-input', 'text', 'title', 'edit-task-title', true)
    titleInput.setAttribute('placeholder', 'Добавить заголовок задачи');
    titleInput.setAttribute('autocomplete', 'off');

    const creationDateString = createSpan('editbar-message', `Дата создания задачи`)

    const optionsGroup = createDiv('editbar-group');

    const checkbox = createCheckbox(false);
    checkbox.classList.add('editbar-checkbox');
    checkbox.addEventListener('click', changeCheckBoxState);

    const deadlineInput = createInput('editbar-date-input', 'text', 'date', 'edit-task-date', false);
    deadlineInput.setAttribute('placeholder', 'Дата выполнения');
    deadlineInput.setAttribute('autocomplete', 'off')
    $(deadlineInput).datepicker(datepickerOptions);

    const button = createButton('important-editbar-btn', 'button', 'importantButton', 'Важное');
    const importantBtn = createImportantButton(button, false);
    importantBtn.addEventListener('click', changeStateImportantButton)

    optionsGroup.append(checkbox, deadlineInput, importantBtn);

    const selectCathegoryLabel = createLabel('editbar-select', `Категории:`);
    selectCathegoryLabel.classList.add('editbar-message')
    const selectCathegory = createCathgoriesSelect();

    const textareaLabel = createLabel('editbar-textarea', `Описание задачи:`);
    textareaLabel.classList.add('editbar-message')
    const textarea = createDescriptionTextarea();

    const buttonsGroup = createDiv('editbar-group')
    const saveBtn = createButton('editbar-btn', 'submit', 'saveButton', 'Сохранить');
    saveBtn.classList.add('editbar-save-btn');
    const deleteBtn = createButton('editbar-btn', 'button', 'deleteButton', 'Удалить');
    deleteBtn.addEventListener('click', deleteTask)
    deleteBtn.classList.add('editbar-delete-btn');
    buttonsGroup.append(saveBtn, deleteBtn);

    form.append(titleInput, creationDateString, optionsGroup, selectCathegoryLabel, selectCathegory,textareaLabel, textarea, buttonsGroup);
    form.addEventListener('submit', saveTask)
    editbar.append(form)

    return editbar;
}

// Event handlers

function changeCheckBoxState(event) {
    event.stopPropagation()

    const checkbox = event.target.closest('.editbar-checkbox');

    if (event.target.classList.contains('fa-square')) {
        event.target.classList.remove('fa-square');
        event.target.classList.add('fa-check-square')
        checkbox.setAttribute('title', 'Добавить в активные');

    } else if (event.target.classList.contains('fa-check-square')) {
        event.target.classList.remove('fa-check-square');
        event.target.classList.add('fa-square')
        checkbox.setAttribute('title', 'Добавить в завершённые');
    }
}

function closeEditbarOnClickOutside(event) {
    const rootContainer = event.target.closest('.root')

    const selectedTask = rootContainer.querySelector('.task-selected');

    const editBar = rootContainer.querySelector('.editbar');
    const sortBtn = rootContainer.querySelector('.sort-btn')
    const hideTaskBtn = rootContainer.querySelector('.taskbar-hide-btn');

    const targetSelectedTask = event.target === selectedTask || event.target.closest('.task-selected')

    const targetEditBar = event.target === editBar || event.target.closest('.editbar');
    const targetSortBtn = event.target === sortBtn;
    const targetHideTaskBtn = event.target === hideTaskBtn;

    if (!targetSelectedTask 
        && !targetEditBar 
        && !targetHideTaskBtn 
        && !targetSortBtn
        ) {
            if (rootContainer.classList.contains('root-with-editbar')) {
                selectedTask.classList.remove('task-selected');
                rootContainer.classList.remove('root-with-editbar');
            }
    }
}

function saveTask(event) {
    event.preventDefault();

    const form = event.target.elements;

    const id = event.target.getAttribute('data-task-id');

    const title = form.title.value;
    const description =form.description.value;

    const parseDate = form.date.value.split('.')
    const [day, month, year] = parseDate;
    const deadline = new Date(`${year}-${month}-${day}`).getTime();

    const checkboxIcon = event.target.querySelector('svg');

    let isDone;
    if (checkboxIcon.classList.contains('fa-check-square')) {
        isDone = true;
    } else {
        isDone = false;
    }

    let isImportant;
    if (form.importantButton.classList.contains('important-btn-selected')) {
        isImportant = true;
    } else {
        isImportant = false;
    }

    const cathegory = [];
    form.cathegories.childNodes.forEach(element => {
        if (element.selected === true) {
            cathegory.push(+element.value)
        }
    });

    const data = {
        id: +id,
        title,
        description,
        deadline,
        isDone,
        isImportant,
        cathegory,
    }

    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }

    fetch(BASEURL + 'task', requestOptions)
        .then(res => {
            if (res.status === 200) {
                const rootContainer = event.target.closest('.root');
                const selectedCathegory = rootContainer.querySelector('.sidebar-selected-cathegory');
    
                changeActiveMenuItem(selectedCathegory)
                rootContainer.classList.remove('root-with-editbar');
            }
        })

}

function deleteTask(event) {

    const id = document.forms.editbarForm.getAttribute('data-task-id');

    const data = {id: +id};

    const requestOptions = {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }

    fetch(BASEURL + 'task', requestOptions)
        .then(res => {
            if (res.status === 200) {
                const rootContainer = event.target.closest('.root');
                const selectedCathegory = rootContainer.querySelector('.sidebar-selected-cathegory');
    
                changeActiveMenuItem(selectedCathegory)
                rootContainer.classList.remove('root-with-editbar');
            }
        })
}