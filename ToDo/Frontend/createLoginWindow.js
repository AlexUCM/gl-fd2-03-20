function createLoginWindow() {

    const loginWindow = createDiv('login');
    const header = createSpan('login-header', 'Добро пожаловать в TODOLIST');
    const form = createForm('login-form', 'loginForm');

    const loginLabel = createLabel('login-login', 'Логин');
    const loginInput = createInput('login-input', 'text', 'login', 'login-login', true);
    const loginPrompt = createSpan('login-prompt', 'Введите имя пользователя');

    const passwordLabel = createLabel('login-password', 'Пароль');
    const passwordInput = createInput('login-input', 'password', 'password', 'login-password', true);
    const passwordPrompt = createSpan('register-prompt', 'Введите пароль от аккаунта');

    const loginButtonsGroup = createDiv('login-buttons-group');
    const loginBtn = createButton('login-submit-btn', 'submit', 'loginBtn', 'Войти');
    const registerBtn = createButton('register-change-btn', 'button', 'registerBtn', 'Зарегистрироваться');
    registerBtn.addEventListener('click', removeLoginWindow);
    loginButtonsGroup.append(loginBtn, registerBtn);

    form.append(loginLabel, loginInput, loginPrompt);
    form.append(passwordLabel, passwordInput, passwordPrompt);
    form.append(loginButtonsGroup);
    form.addEventListener('submit', setLoginActions);
    loginWindow.append(header, form);

    return loginWindow;
}

// Event handlers

function removeRegisterWindow(event) {
    const registerWindow = event.target.closest('.register');
    registerWindow.remove();
    const body = document.querySelector('body');
    body.prepend(createLoginWindow());
}

function removeLoginWindow(event) {
    const loginWindow = event.target.closest('.login');
    loginWindow.remove();
    const body = document.querySelector('body');
    body.prepend(createRegisterWindow());
}

function setLoginActions(event) {
    event.preventDefault();
    
    const form = event.target.elements;

    const data = {
        login: form.login.value,
        password: form.password.value
    };

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }

    fetch(BASEURL + 'login', requestOptions)
        .then(res => {
            const prevMessage = event.target.querySelector('.login-message');
            if (prevMessage) {
                prevMessage.remove();
            }
            if (res.status === 202) {
                const loginWindow = event.target.closest('.login');
                loginWindow.remove();
                loginActions();
            } else {
                const incorrectLoginMessage = createSpan('login-message', 'Неправильный логин или пароль');
                event.target.append(incorrectLoginMessage);
            }
        })
}

function registerNewUser(event) {
    event.preventDefault();
    
    const form = event.target.elements;

    const data = {
        name: form.name.value,
        login: form.login.value,
        password: form.password.value
    };

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }

    fetch(BASEURL + 'register', requestOptions)
        .then(res => {
            const prevMessage = event.target.querySelector('.login-message');
            if (prevMessage) {
                prevMessage.remove();
            }
            if (res.status === 201) {
                const registerWindow = event.target.closest('.register');
                registerWindow.remove();
                const body = document.querySelector('body');
                body.prepend(createLoginWindow());

                const sucsessRegisterMessage = createSpan('login-message', 'Спасибо за регистрацию! Вы можете войти в систему, используя свой логин и пароль.');
                const loginForm = document.querySelector('[name="loginForm"]')
                loginForm.append(sucsessRegisterMessage);
            }
            if (res.status === 409) {
                const incorrectLoginMessage = createSpan('login-message', 'Введённый логин уже занят, попробуйте другой');
                event.target.append(incorrectLoginMessage);
            }
        })
}
