function createDiv(elemClass) {
    const div = document.createElement('div');
    div.classList.add(elemClass);

    return div;
}

function createSpan(elemClass, text) {
    const span = document.createElement('span');
    span.classList.add(elemClass);
    span.textContent = text;

    return span;
}

function createUl(elemClass) {
    const ul = document.createElement('ul');
    ul.classList.add(elemClass);

    return ul;
}

function createLi(elemClass) {
    const li = document.createElement('li');
    li.classList.add(elemClass);

    return li;
}

function createLabel(targetElem, text) {
    const label = document.createElement('label');
    label.setAttribute('for', targetElem);
    label.textContent = text;

    return label;
}

function createInput(elemClass, elemType, elemName, elemId, isRequired) {
    const input = document.createElement('input');
    input.classList.add(elemClass)
    input.setAttribute('type', elemType);
    input.setAttribute('name', elemName);
    input.setAttribute('id', elemId);
    if (isRequired) {
        input.setAttribute('required', 'required');
    }

    return input;
}

function createForm(elemClass, elemName) {
    const form = document.createElement('form');
    form.classList.add(elemClass)
    form.setAttribute('name', elemName);

    return form;
}

function createButton(elemClass, elemType, elemName, title) {
    const button = document.createElement('button');
    button.classList.add(elemClass);
    button.setAttribute('type', elemType);
    if (elemName) {
        button.setAttribute('name', elemName);
    }
    button.textContent = title;

    return button;
}

function createFAIcon(firstClass, secondClass) {
    const icon = document.createElement('i');
    icon.classList.add(firstClass, secondClass);

    return icon;
}

function createImportantButton(button, isImportant) {
    button.classList.add('important-btn')
    if (isImportant) {
        button.classList.add('important-btn-selected');
        button.setAttribute('title','Удалить из важных')
    } else {
        button.setAttribute('title','Добавить в важные')
    }
    
    return button;
}

function createSortMenuItem(elemClass, elemId, title) {
    const item = document.createElement('li');
    item.classList.add(elemClass);
    item.setAttribute('data-sort', elemId);
    item.textContent = title;

    return item;
}

function setTextOnSortButton(sortType, button) {
    if (sortType === 'byCreationDate') {
        button.textContent = 'Сортировать: По дате создания'
    }
    if (sortType === 'byDeadline') {
        button.textContent = 'Сортировать: По дате выполнения'
    }
    if (sortType === 'byName') {
        button.textContent = 'Сортировать: По алфавиту'
    }

    return button;
}

function сreateSortButton() {
    const sortButton = createButton('sort-btn', null);

    fetch(BASEURL + 'options', {credentials: 'include'})
        .then(res => res.json())
        .then(options => {
            setTextOnSortButton(options.sortType, sortButton);
            sortButton.addEventListener('click', showSortMenu);
        })

    return sortButton;
}


function createCathegoryItem(elemId, title) {
    const item = createLi('sidebar-cathegory');

    item.setAttribute('id', elemId);
    const itemTitle = createSpan('sidebar-cathegory-title', title);
    item.append(itemTitle);

    return item;
}

function createCustomCathegoryItem(elemId, title) {
    const item = createCathegoryItem(elemId, title)
    item.classList.add('sidebar-custom-cathegory');

    const deleteButton = createDiv('sidebar-delete-btn');
    const deleteIcon = createFAIcon('fas', 'fa-times');
    deleteButton.append(deleteIcon);
    deleteButton.addEventListener('click', deleteCustomCathegory)
    
    item.append(deleteButton)
    item.addEventListener('click', createTaskList);

    return item;
}

function createDefaultCathegoriesList() {
    const cathegoriesList = createUl('sidebar-default-cathegories');

    const allTasks = createCathegoryItem('all-tasks', 'Все задачи');
    allTasks.addEventListener('click', createTaskList);
    const todayTasks = createCathegoryItem('today-tasks', 'Сегодня');
    todayTasks.addEventListener('click', createTaskList);
    const weekTasks = createCathegoryItem('week-tasks', 'Неделя');
    weekTasks.addEventListener('click', createTaskList);
    const monthTasks = createCathegoryItem('month-tasks', 'Месяц');
    monthTasks.addEventListener('click', createTaskList);
    const yearTasks = createCathegoryItem('year-tasks', 'Год');
    yearTasks.addEventListener('click', createTaskList);
    const importantTasks = createCathegoryItem('important-tasks', 'Важное');
    importantTasks.addEventListener('click', createTaskList);

    cathegoriesList.append(allTasks, importantTasks, todayTasks, weekTasks, monthTasks, yearTasks);

    return cathegoriesList;
}

function createCustomCathegoriesList() {
    const cathegoriesList = createUl('sidebar-custom-cathegories');

    fetch(BASEURL + 'cathegories', {credentials: 'include'})
        .then(res => res.json())
        .then(customCathegories => {
            customCathegories.forEach(cathegory => {
                const cathegoryItem = createCustomCathegoryItem(cathegory.id, cathegory.title);
                cathegoriesList.append(cathegoryItem);
            })
        })

    return cathegoriesList;
}

function createTask(elemClass, id) {
    const task = createLi('task')
    task.classList.add(elemClass);
    task.setAttribute('data-id', id);

    return task;
}

function createCheckbox(isChecked) {
    const checkbox = createDiv('task-checkbox');

    if (isChecked) {
        checkbox.setAttribute('title', 'Добавить в активные');
        const checkedIcon = createFAIcon('far','fa-check-square');
        checkbox.append(checkedIcon)
    } else {
        checkbox.setAttribute('title', 'Добавить в завершённые');
        const uncheckedIcon = createFAIcon('far','fa-square');
        checkbox.append(uncheckedIcon)
    }

    return checkbox;
}

function createDeadlineString(elemClass, deadline) {
    const deadlineString = document.createElement('span');
    deadlineString.classList.add(elemClass)
    deadlineString.setAttribute('title', 'Дата выполнения')
    const deadlineDay = new Date(deadline).getDate();
    const deadlineMonth = new Date(deadline).getMonth() + 1;
    const deadlineYear = new Date(deadline).getFullYear();
    deadlineString.textContent = `${deadlineDay}.${deadlineMonth}.${deadlineYear}`;

    const currentDay = new Date().getDate();
    const currentMonth = new Date().getMonth();
    const currentYear = new Date().getFullYear();
    const today = new Date(currentYear, currentMonth, currentDay)

    if (new Date(deadline) < today) {
        deadlineString.classList.add('task-deadline-expired');
    }

    return deadlineString;
}

function createCathegoryString(cathegoryTitle) {
    const cathegoryString = createSpan('task-cathegory', cathegoryTitle);
    cathegoryString.setAttribute('title', 'Категория задачи')

    return cathegoryString;
}


function createCathegoriesList(cathegories) {
    const cathegoriesList = createDiv('task-group');

    cathegories.forEach(cathegoryId => {
        fetch(BASEURL + 'cathegory/?id=' + cathegoryId, {credentials: 'include'})
            .then(res => res.json())
            .then(cathegory => {
                const cathegoryString = createCathegoryString(cathegory.title);
                cathegoriesList.append(cathegoryString);
            })
    })

    return cathegoriesList;
}

function createCathgoriesSelect() {
    const select = document.createElement('select');
    select.classList.add('editbar-select');
    select.setAttribute('name', 'cathegories')
    select.setAttribute('id', 'editbar-select');
    select.setAttribute('multiple', 'multiple');

    fetch(BASEURL + 'cathegories', {credentials: 'include'})
        .then(res => res.json())
        .then(customCathegories => {
            customCathegories.forEach(cathegory => {
                const option = createCathegoriesSelectOptions(cathegory);
                select.append(option);
            })
            select.setAttribute('size', select.length);
    })
    
    return select;
}

function createCathegoriesSelectOptions(cathegory) {
    const option = document.createElement('option');
    option.textContent = cathegory.title;
    option.setAttribute('value', cathegory.id)

    return option;
}

function createDescriptionTextarea() {
    const textarea = document.createElement('textarea');
    textarea.classList.add('editbar-textarea');
    textarea.setAttribute('id', 'editbar-textarea')
    textarea.setAttribute('name', 'description');
    textarea.setAttribute('rows', '20');
    textarea.setAttribute('placeholder', 'Добавить описание задачи');

    return textarea;
}

function createRootContainer() {
    const container = document.createElement('div');
    container.classList.add('root');
    container.addEventListener('click', closeSortMenuOnClickOutside);
    container.addEventListener('click', closeEditbarOnClickOutside);

    return container;
}
