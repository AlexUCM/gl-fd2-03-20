function createActiveTask(task) {

    const taskContainer = createTask('task-active', task.id);

    const leftGroup = createDiv('task-group');

    const checkbox = createCheckbox(false)
    checkbox.addEventListener('click', setCheckboxAction);
    leftGroup.append(checkbox);
    if (task.deadline) {
        const deadlineString = createDeadlineString('task-deadline', task.deadline);
        leftGroup.append(deadlineString);
    }
    const title = createSpan('task-title', task.title);
    leftGroup.append(title);

    const rightGroup = createDiv('task-group');

    if (task.cathegory.length > 0) {
        const cathegoiresList = createCathegoriesList(task.cathegory);
        rightGroup.append(cathegoiresList);
    }
    const button = createButton(null, 'button', null, 'Важное')
    const importantButton = createImportantButton(button, task.isImportant);
    importantButton.addEventListener('click', setImportantButtonAction)
    rightGroup.append(importantButton);

    taskContainer.append(leftGroup, rightGroup);
    taskContainer.addEventListener('click', sendDataToEditbar);

    return taskContainer;
}

function createUnactiveTask(task) {

    const taskContainer = createTask('task-unactive', task.id);
    
    const leftGroup = createDiv('task-group');

    const checkbox = createCheckbox(true);
    checkbox.addEventListener('click', setCheckboxAction);
    leftGroup.append(checkbox);
    if (task.deadline) {
        const deadlineString = createDeadlineString('task-deadline', task.deadline);
        leftGroup.append(deadlineString);
    }
    const title = createSpan('task-title', task.title);
    title.classList.add('task-done-title');
    leftGroup.append(title);

    const rightGroup = createDiv('task-group');

    if (task.cathegory.length > 0) {
        const cathegoiresList = createCathegoriesList(task.cathegory);
        rightGroup.append(cathegoiresList);
    }

    const button = createButton(null, 'button', null, 'Важное')
    const importantButton = createImportantButton(button, task.isImportant);
    importantButton.addEventListener('click', setImportantButtonAction);
    rightGroup.append(importantButton);

    taskContainer.append(leftGroup, rightGroup);
    taskContainer.addEventListener('click', sendDataToEditbar)

    return taskContainer;
}

// Event handlers

function sendDataToEditbar(event) {

    const checkedTask = document.querySelector('.task-selected');
    if (checkedTask) {
        checkedTask.classList.remove('task-selected');
    }
    const container = event.target.closest('.task');
    container.classList.add('task-selected')
    const id = container.getAttribute('data-id');

    fetch(BASEURL + 'task/?id=' + id, {credentials: 'include'})
        .then(res => res.json())
        .then(task => {
            const root = event.target.closest('.root');
            if (!root.classList.contains('root-with-editbar')) {
                root.classList.add('root-with-editbar');
            }
            const editbarForm = document.forms.editbarForm;
            editbarForm.setAttribute('data-task-id', task.id)

            const form = editbarForm.elements;

            form.title.value = task.title;

            const creationDateString = editbarForm.querySelector('.editbar-message');  
            const creationDate = new Date(task.creationDate);
            const creationDay = creationDate.getDate();
            const creationMonth = creationDate.getMonth() + 1;
            const creationYear = creationDate.getFullYear();
            creationDateString.textContent = `Задача создана: ${creationDay}.${creationMonth}.${creationYear}`

            if (task.deadline) {
                const deadline = new Date(task.deadline);
                const deadlineDay = deadline.getDate();
                const deadlineMonth = deadline.getMonth() + 1;
                const deadlineYear = deadline.getFullYear();

                const deadlineString = `${deadlineDay}.${deadlineMonth}.${deadlineYear}`
                $(form.date).datepicker("setDate", deadlineString)
            } else {
                $(form.date).datepicker("setDate", '')
            }

            if (task.isImportant) {
                form.importantButton.classList.add('important-btn-selected');
                form.importantButton.setAttribute('title','Удалить из важных');
            } else {
                form.importantButton.classList.remove('important-btn-selected');
                form.importantButton.setAttribute('title','Добавить в важные');
            }

            const oldCheckbox = editbarForm.querySelector('.editbar-checkbox');
            oldCheckbox.remove();
            const newCheckbox = createCheckbox(task.isDone);
            newCheckbox.classList.add('editbar-checkbox');
            newCheckbox.addEventListener('click', changeCheckBoxState);
            form.date.before(newCheckbox);

            form.cathegories.childNodes.forEach(element => {
                element.selected = false;
                if (task.cathegory.find(cathegory => cathegory === +element.value)) {
                    element.selected = true;
                };
            });

            form.description.textContent = task.description;
        })
}

function setImportantButtonAction(event) {
    event.stopPropagation()

    const container = event.target.closest('.task');
    const id = container.getAttribute('data-id');

    const rootContainer = container.closest('.root')
    if (rootContainer.classList.contains('root-with-editbar')) {
        rootContainer.classList.remove('root-with-editbar')
    }

    fetch(BASEURL + 'task/?id=' + id, {credentials: 'include'})
        .then(res => res.json())
        .then(invertTaskImportantce)
}

function invertTaskImportantce ({id, isImportant}) {

    const data = {
        id,
        isImportant: !isImportant
    }

    const requestOptions = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }
    
    fetch(BASEURL + 'task', requestOptions)
        .then(res => {
            if (res.status === 200) {
                const selectedCathegory = document.querySelector('.sidebar-selected-cathegory')
                changeActiveMenuItem(selectedCathegory)
            }
        })   
};

function setCheckboxAction(event) {
    event.stopPropagation()

    const container = event.target.closest('.task');
    const id = container.getAttribute('data-id');

    const rootContainer = container.closest('.root')
    if (rootContainer.classList.contains('root-with-editbar')) {
        rootContainer.classList.remove('root-with-editbar')
    }

    fetch(BASEURL + 'task/?id=' + id, {credentials: 'include'})
        .then(res => res.json())
        .then(({id, title, description, creationDate, deadline, isDone: doneStatus, isImportant, cathegory}) => {

            const data = {
                id,
                title,
                description,
                creationDate,
                deadline,
                isDone: !doneStatus,
                isImportant,
                cathegory
            };
        
            const requestOptions = {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include',
                body: JSON.stringify(data)
            }
            
            fetch(BASEURL + 'task', requestOptions)
                .then(res => {
                    if (res.status === 200) {
                        const selectedCathegory = document.querySelector('.sidebar-selected-cathegory')
                        changeActiveMenuItem(selectedCathegory)
                    }
                })   
        })
}