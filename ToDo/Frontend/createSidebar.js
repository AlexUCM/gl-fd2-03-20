function createSideBar() {

    const sidebar = createDiv('sidebar');

    const title = createSpan('sidebar-title', 'Списки');

    const defaultCathegoriesList = createDefaultCathegoriesList();
    const customCathegoriesList =  createCustomCathegoriesList();

    const addTaskForm = document.createElement('form');
    const addTaskInput = createInput('sidebar-input', 'text', 'addCathegory');
    addTaskInput.setAttribute('placeholder', 'Добавить категорию');
    addTaskInput.setAttribute('autocomplete', 'off')

    addTaskForm.append(addTaskInput);
    addTaskForm.addEventListener('submit', addCustomCathegory)

    sidebar.append(title, defaultCathegoriesList, addTaskForm, customCathegoriesList);

    return sidebar;
}

// Event handlers

function addCustomCathegory(event) {
    event.preventDefault();

    const form = event.target.elements;
    
    const data = {
        id: Date.now(),
        title: form.addCathegory.value
    }

    const requestOptions = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }

    fetch(BASEURL + 'cathegory', requestOptions)
        .then(res => {
            if(res.status === 201) {
                form.addCathegory.value = null;
                form.addCathegory.blur();
                const sidebar = event.target.closest('.sidebar');
                const customCathegoriesList = sidebar.querySelector('.sidebar-custom-cathegories');
                const customCathegoryItem = createCustomCathegoryItem(data.id, data.title);
                customCathegoriesList.append(customCathegoryItem);

                const rootContainer = sidebar.closest('.root');
                const editbar = document.querySelector('.editbar');
                editbar.remove();
                rootContainer.append(createEditBar())
            }
        })
}

function setActiveCahtegoryToDefault() {
    const allTasks = document.querySelector('#all-tasks');
    changeActiveMenuItem(allTasks)
}

function deleteCustomCathegory(event) {
    event.stopPropagation();

    const cathegoryContainer = event.target.closest('.sidebar-cathegory');
    const cathegoryId  = cathegoryContainer.getAttribute('id');
    const cathegoryTitle = cathegoryContainer.querySelector('.sidebar-cathegory-title');

    const titleString = cathegoryTitle.textContent;

    const modalWindow = document.createElement('div');
    modalWindow.setAttribute('id', 'dialog-confirm');
    modalWindow.setAttribute('title', 'Удалить категорию?')

    const message = document.createElement('p');
    message.textContent = `Вы действительно хотите удалить категорию "${titleString}"?`
    
    modalWindow.append(message);

    $(modalWindow).dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
            "Удалить": function() {

                const data = {id: +cathegoryId}
            
                const requestOptions = {
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    credentials: 'include',
                    body: JSON.stringify(data)
                }
            
                fetch(BASEURL + 'cathegory', requestOptions)
                    .then(res => {
                        if(res.status === 200) {
                            if (cathegoryContainer.classList.contains('sidebar-selected-cathegory')) {
                                setActiveCahtegoryToDefault();
                            } else {
                                const sidebar = event.target.closest('.sidebar');
                                const selectedCathegory = sidebar.querySelector('.sidebar-selected-cathegory')
                                changeActiveMenuItem(selectedCathegory);
                            }
                            const rootContainer = event.target.closest('.root');
                            if (rootContainer.classList.contains('root-with-editbar')) {
                                rootContainer.classList.remove('root-with-editbar');
                            }
                            const editbar = rootContainer.querySelector('.editbar');
                            editbar.remove();
                            rootContainer.append(createEditBar());
                            cathegoryContainer.remove();
                            $(this).dialog("close");
                        }
                    })
            },
            "Отмена": function() {
                $(this).dialog("close");
            }
        }
    })
}

function createTaskList(event) {
    const container = event.target.closest('.sidebar-cathegory');
    changeActiveMenuItem(container);
}

function changeActiveMenuItem(menuItem) {
    
    const sidebar = document.querySelector('.sidebar');
    const cathegories = sidebar.querySelectorAll('.sidebar-cathegory');
    cathegories.forEach(cathegory => cathegory.classList.remove('sidebar-selected-cathegory'));

    menuItem.classList.add('sidebar-selected-cathegory');
    const menuItemId = menuItem.getAttribute('id');
    
    const listHeader = document.querySelector('.optbar-cathegory-title');
    listHeader.textContent = menuItem.textContent;

    const requestUrl = getRequestUrl(menuItemId);
    getTaskList(requestUrl);
}

function getRequestUrl(id) {
    switch (id) {
        case 'all-tasks':
            return  `${BASEURL}tasks`;
        case 'today-tasks':
            return `${BASEURL}tasks/?deadline=today`;
        case 'week-tasks':
            return `${BASEURL}tasks/?deadline=week`;
        case 'month-tasks':
            return `${BASEURL}tasks/?deadline=month`;
        case 'year-tasks':
            return `${BASEURL}tasks/?deadline=year`;
        case 'important-tasks':
            return `${BASEURL}tasks/?important=true`;
        default:
            return `${BASEURL}tasks/?cathegory=${id}`;
    }
}

function getTaskList(url) {
    fetch(url, {credentials: 'include'})
        .then(res => res.json())
        .then(tasks => renderTasks(tasks))
}

function renderTasks(tasks) {

    const taskbar = document.querySelector('.taskbar')
    const prevActiveUl= taskbar.querySelector('.taskbar-active-tasks');
    const prevUnactiveUl = taskbar.querySelector('.taskbar-unactive-tasks');

    prevActiveUl.remove();
    prevUnactiveUl.remove();

    const activeUl = createUl('taskbar-active-tasks');
    const unactiveUl = createUl('taskbar-unactive-tasks');

    const hideTasksButton = taskbar.querySelector('.taskbar-hide-btn')
    const stateOfHideTaskButton = hideTasksButton.classList.contains('taskbar-hide-btn-active');
    if (stateOfHideTaskButton) {
        unactiveUl.classList.add('taskbar-unactive-tasks-visible');
    }

    const noActiveTasksMessage = createSpan('taskbar-message', 'Активные задачи отсутствуют');
    const noUnactiveTasksMessage = createSpan('taskbar-message', 'Завершённые задачи отсутствуют');

    if (tasks.length > 0) {

        const activeTasks = tasks.filter(task => task.isDone === false);
        if (activeTasks.length > 0) {
            activeTasks.forEach(task => activeUl.append(createActiveTask(task)));
        } else {
            activeUl.append(noActiveTasksMessage);
        }

        const unactiveTasks = tasks.filter(task => task.isDone === true);
        if (unactiveTasks.length > 0) {
            unactiveTasks.forEach(task => unactiveUl.append(createUnactiveTask(task)));
        } else {
            unactiveUl.append(noUnactiveTasksMessage);
        }

    } else {
        activeUl.append(noActiveTasksMessage);
        unactiveUl.append(noUnactiveTasksMessage);
    }

    hideTasksButton.before(activeUl);
    hideTasksButton.after(unactiveUl);
}