function createRegisterWindow() {

    const registerWindow = createDiv('register');
    const header = createSpan('register-header', 'Регистрация в системе TODOLIST')
    const form = createForm('register-form', 'registerForm');

    const nameLabel = createLabel('regName', 'Имя пользователя');
    const nameInput = createInput('login-input', 'text', 'name', 'regName', true);
    const namePrompt = createSpan('register-prompt', 'Введите имя пользователя');

    const loginLabel = createLabel('reglogin', 'Логин');
    const loginInput = createInput('login-input', 'text', 'login', 'reglogin', true);
    const loginPrompt = createSpan('register-prompt', 'Введите логин');

    const passwordLabel = createLabel('regPassword', 'Пароль');
    const passwordInput = createInput('login-input', 'password', 'password', 'regPassword', true);
    const passwordPrompt = createSpan('register-prompt', 'Введите пароль от аккаунта');

    const registerButtonsGroup = createDiv('register-buttons-group');
    const registerBtn = createButton('register-submit-btn', 'submit', 'registerBtn', 'Зарегистрироваться');
    const loginBtn = createButton('login-change-btn', 'button', 'loginBtn', 'Войти');
    loginBtn.addEventListener('click', removeRegisterWindow)
    registerButtonsGroup.append(loginBtn, registerBtn);
    
    form.append(nameLabel, nameInput, namePrompt);
    form.append(loginLabel, loginInput, loginPrompt);
    form.append(passwordLabel, passwordInput, passwordPrompt);
    form.append(registerButtonsGroup);
    form.addEventListener('submit', registerNewUser);
    registerWindow.append(header, form);

    return registerWindow;
}