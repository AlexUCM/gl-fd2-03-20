function createLogo () {

    const logo = createDiv('logo');
    
    const logoTitle = createSpan('logo-title', 'TODOLIST');
    logoTitle.addEventListener('click', setActiveCahtegoryToDefault)

    logo.append(logoTitle);

    return logo;
}