// Import modules

const authDb = require('./authentication.json');
const mainDb = require('./main.json');
const json = require('body-parser').json;
const urlencoded = require('body-parser').urlencoded;
const cors = require('cors');
const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');

// Add main middleware

app.use(cors({
    origin: [
        'http://127.0.0.1:8080',
        'http://localhost:8080',
        'http://192.168.1.42:8080'
    ],
    credentials: true
    }));
app.use(json());
app.use(urlencoded({extended: true}));
app.use(cookieParser());

// Launch server

app.listen(3000, () => console.log('Server listening on port 3000!'));

// Users routes

app.post('/register', registerNewUser);
app.post('/login', login);
app.post('/logout', logout);

// JWT access middleware

app.use(verifyToken);

// Login check route

app.post('/check-auth', checkAuth);

// Tasks routes 

app.get('/task', getTask);
app.get('/tasks', getTasks);
app.post('/task', postTask);
app.put('/task', putTask);
app.delete('/task', deleteTask);

// Options routes

app.get('/cathegory', getCathegory);
app.get('/cathegories', getCathegories);
app.post('/cathegory', postCathegory);
app.put('cathegory', putCathegory);
app.delete('/cathegory', deleteCathegory);

app.get('/options', getOptions);
app.put('/options', setOptions);

// Auth functions

function registerNewUser(req, res) {
    const {name, login, password} = req.body;

    const takenLogin = authDb.find(user => user.login === login)

    if (takenLogin) {
        res.sendStatus(409)
    } else {
        const userId = Date.now();

        const newUser = {
            id: userId,
            name,
            login,
            password
        }
    
        authDb.push(newUser);

        const newUserDataIten = {
            userId,
            options: {
                showDoneTasks: false,
                sortType: "byCreationDate"
            },
            cathegories: [],
            tasks: []
        }

        mainDb.push(newUserDataIten);

        res.sendStatus(201);
    }
};

function login(req, res) {
    const {login, password} = req.body;

    const userFromDb = authDb.find(user => {
        return (user.login === login && user.password === password);
    });

    if (userFromDb) {
        const userDataForToken = {
            id: userFromDb.id
        }
    
        const tokenExpiration = '1d';
        const token = jwt.sign(userDataForToken, 'secretkey', {
            expiresIn: tokenExpiration
        });

        const cookieExpiration = new Date(Date.now() + 2 * 60 * 60 * 1000);
        res
            .cookie('jwtoken', token, {
                expires: cookieExpiration,
                httpOnly: true
            })
            .sendStatus(202);

    } else {
        res
            .clearCookie('jwtoken')
            .sendStatus(401);
    }
};

function logout(req, res) {
    res
        .clearCookie('jwtoken')
        .sendStatus(200);
};

// JWT middleware functions

function verifyToken(req, res, next) {
    const {jwtoken} = req.cookies;

    if (typeof jwtoken !== 'undefined') { 
        jwt.verify(jwtoken, 'secretkey', (err, authData) => {
            if (err) {
                res.sendStatus(403);
            } else {
                req.authData = authData;
                next();
            }
        })
    } else {
        res.sendStatus(403);
    }
}

// Login check function

function checkAuth(req, res) {
    res.sendStatus(202)
}

// Task functions 

function getTask(req, res) {
    const userId = req.authData.id;
    const taskId = parseInt(req.query.id);

    const userData = mainDb.find(userData => userData.userId === userId);
    const task = userData.tasks.find(task => task.id === taskId);
    res.json(task);
}

function getTasks(req, res) {
    const userId = req.authData.id;
    const userData = mainDb.find(userData => userData.userId === userId);
    const sortType = userData.options.sortType;

    const {deadline, important, cathegory} = req.query;

    if (deadline) {
        const fiteredDeadlineTasks = getFilteredTasksByDate(deadline, userData.tasks);
        const sortedDeadlineTasks = sortTasks(fiteredDeadlineTasks, sortType);
        res.json(sortedDeadlineTasks);
    } else if (important) {
        const fiteredImportantTasks = userData.tasks.filter(task => task.isImportant);
        const sortedImportantTasks = sortTasks(fiteredImportantTasks, sortType);
        res.json(sortedImportantTasks);
    } else if (cathegory) {
        const filteredCathegoryTasks = userData.tasks.filter(task => task.cathegory.includes(+cathegory));
        const sortedCathegoryTasks = sortTasks(filteredCathegoryTasks, sortType);
        res.json(sortedCathegoryTasks);
    } else {
        const sortedAllTasks = sortTasks(userData.tasks, sortType);
        res.json(sortedAllTasks);
    }
}

function getFilteredTasksByDate(deadline, taskList) {
    switch(deadline) {
        case 'today':
            return taskList
                .filter(task => new Date(task.deadline).getFullYear() === new Date().getFullYear())
                .filter(task => new Date(task.deadline).getMonth() === new Date().getMonth())
                .filter(task => new Date(task.deadline).getDate() === new Date().getDate());
        case 'week':
            return taskList
                .filter(task => {
                    const currentDay = new Date().getDate();
                    const dateInAWeek = new Date().setDate(currentDay + 7);
                    return (task.deadline > Date.now() && task.deadline < dateInAWeek);
                });
        case 'month':
            return taskList
                .filter(task => {
                    const currentMonth = new Date().getMonth();
                    const dateInAMonth = new Date().setMonth(currentMonth + 1);
                    return (task.deadline > Date.now() && task.deadline < dateInAMonth);
                });
        case 'year':
            return taskList
                .filter(task => {
                    const currentYear = new Date().getFullYear();
                    const dateInAYear = new Date().setFullYear(currentYear + 1);
                    return (task.deadline > Date.now() && task.deadline < dateInAYear);
                });
    }
}

function sortTasks(tasks, sortType) {
    if (sortType === 'byCreationDate') {
        return tasks.sort((a, b) => a['creationDate'] > b['creationDate'] ? 1 : -1);
    }
    if (sortType === 'byDeadline') {
        return tasks.sort((a, b) => a['deadline'] > b['deadline'] ? 1 : -1);
    }
    if (sortType === 'byName') {
        return tasks.sort((a, b) => a['title'] > b['title'] ? 1 : -1);
    }
}

function postTask(req, res) {
    const userId = req.authData.id;
    const taskId = req.body.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const task = userData.tasks.find(task => task.id === taskId);

    if (task) {
        res.sendStatus(409);
    } else {
        const newTask = {
            title: null,
            description: null,
            deadline: null,
            isDone: false,
            isImportant: false,
            cathegory: [],
            ...req.body
        }
        
        userData.tasks.push(newTask);
        res.sendStatus(201); 
    }
}

function putTask(req, res) {
    const userId = req.authData.id;
    const taskId = req.body.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const task = userData.tasks.find(task => task.id === taskId);

    if (task) {
        const updatedTask = {...task, ...req.body};
        const taskIndex = userData.tasks.indexOf(task);
        userData.tasks.splice(taskIndex, 1, updatedTask);
        res.sendStatus(200);
    } else {
        res.sendStatus(404);
    }
}

function deleteTask(req, res) {
    const userId = req.authData.id;
    const taskId = req.body.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const task = userData.tasks.find(task => task.id === taskId);

    if (task) {
        const taskIndex = userData.tasks.indexOf(task);
        userData.tasks.splice(taskIndex, 1)
        res.sendStatus(200);
    } else {
        res.sendStatus(404);
    }
}

// Options functions

function getCathegories(req, res) {
    const userId = req.authData.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    res.json(userData.cathegories)
}

function getCathegory(req, res) {
    const userId = req.authData.id;
    const cathegoryId = req.query.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const cathegory = userData.cathegories.find(cathegory => cathegory.id === +cathegoryId);
    res.json(cathegory);
}

function postCathegory(req, res) {
    const userId = req.authData.id;
    const cathegoryId = req.body.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const cathegory = userData.cathegories.find(cathegory => cathegory.id === cathegoryId);
    
    if (cathegory) {
        res.sendStatus(409);
    } else {
        const newCathegory = {...req.body}
        
        userData.cathegories.push(newCathegory);
        res.sendStatus(201); 
    }
}

function putCathegory(req, res) {
    const userId = req.authData.id;
    const cathegoryId = req.body.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const cathegory = userData.cathegories.find(cathegory => cathegory.id === cathegoryId);
    
    if (cathegory) {
        const updatedCathegory = {...cathegory, ...req.body};
        const cathegoryIndex = task.cathegory.indexOf(cathegory.id);
        userData.tasks.splice(cathegoryIndex, 1, updatedCathegory);
        res.sendStatus(200);
    } else {
        res.sendStatus(404);
    }
}

function deleteCathegory(req, res) {
    const userId = req.authData.id;
    const cathegoryId = req.body.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    const cathegory = userData.cathegories.find(cathegory => cathegory.id === cathegoryId);

    if(cathegory) {
        const taskIndex = userData.cathegories.indexOf(cathegory);
        userData.cathegories.splice(taskIndex, 1);

        userData.tasks.forEach(task => {
            const cathegoryIndex = task.cathegory.indexOf(cathegoryId);
            if (cathegoryIndex !== -1) {
                task.cathegory.splice(cathegoryIndex, 1);
            }
        })
        res.sendStatus(200);
    } else {
        res.sendStatus(404);
    }
}

function getOptions(req, res) {
    const userId = req.authData.id;

    const userData = mainDb.find(userData => userData.userId === userId);
    res.json(userData.options)
}

function setOptions(req, res) {
    const userId = req.authData.id;

    const userData = mainDb.find(userData => userData.userId === userId);

    const prevOptions = userData.options
    userData.options = {
        ...prevOptions,
        ...req.body
    }
    res.sendStatus(200);

}